package com.ambitioussoftware.k53master.util;

import android.app.Activity;
import android.app.Instrumentation;
import android.util.Log;

import com.google.android.apps.common.testing.testrunner.ActivityLifecycleMonitorRegistry;
import com.google.android.apps.common.testing.testrunner.Stage;

import java.util.Collection;

/**
 * ActivityHelper
 *
 * @author Dmitri De Klerk <dmitriwarren@gmail.com>
 * @since 27 Oct 2014
 */
public class ActivityHelper {

    public static Activity getCurrentActivity(Instrumentation instrumentation) {
        final Activity[] currentActivity = new Activity[1];
        instrumentation.runOnMainSync(
            new Runnable() {
                public void run() {
                    Collection<Activity> resumedActivities
                        = ActivityLifecycleMonitorRegistry
                        .getInstance().getActivitiesInStage(Stage.RESUMED);
                    for (Activity act : resumedActivities) {
                        Log.d(
                            "Your current activity: ",
                            act.getClass().getName()
                        );
                        currentActivity[0] = act;
                        break;
                    }
                }
            }
        );

        return currentActivity[0];
    }
}
