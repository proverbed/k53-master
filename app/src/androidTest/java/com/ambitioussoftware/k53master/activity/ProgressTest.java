package com.ambitioussoftware.k53master.activity;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;

import com.ambitioussoftware.k53master.util.ActivityHelper;
import com.ambitioussoftware.k53master.R;
import com.robotium.solo.Solo;
import com.squareup.spoon.Spoon;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.click;
import static com.google.android.apps.common.testing.ui.espresso.assertion.ViewAssertions.matches;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.isDisplayed;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withText;

@LargeTest
public class ProgressTest extends ActivityInstrumentationTestCase2<MainActivity> {
    MainActivity act;

    private Solo solo;

    @SuppressWarnings("deprecation")
    public ProgressTest() {
        // This constructor was deprecated - but we want to support lower API levels.
        super(
            "com.ambitioussoftware.k53master",
            MainActivity.class
        );
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        // Espresso will not launch our activity for us, we must launch it via getActivity().
        act = getActivity();
        solo = new Solo(getInstrumentation(), getActivity());
    }

    public void testProgress() {
        Spoon.screenshot(
            act,
            "Main-Activity-Screenshot"
        );

        onView(withId(R.id.progressCard))
            .perform(click());

        Activity act = ActivityHelper.getCurrentActivity(getInstrumentation());
        assertEquals(
            "com.ambitioussoftware.k53master.activity.ProgressActivity",
            act.getClass().getName()
        );
        Spoon.screenshot(
            act,
            "Progress-Activity-Screenshot"
        );

    }

    @Override
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

}
