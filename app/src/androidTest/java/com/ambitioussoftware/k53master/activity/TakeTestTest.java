package com.ambitioussoftware.k53master.activity;

import android.app.Activity;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;

import com.ambitioussoftware.k53master.R;
import com.ambitioussoftware.k53master.util.ActivityHelper;
import com.robotium.solo.Solo;
import com.squareup.spoon.Spoon;

import static com.google.android.apps.common.testing.ui.espresso.Espresso.onView;
import static com.google.android.apps.common.testing.ui.espresso.action.ViewActions.click;
import static com.google.android.apps.common.testing.ui.espresso.assertion.ViewAssertions.matches;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.isDisplayed;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withId;
import static com.google.android.apps.common.testing.ui.espresso.matcher.ViewMatchers.withText;

@LargeTest
public class TakeTestTest extends ActivityInstrumentationTestCase2<MainActivity> {
    MainActivity act;

    private Solo solo;

    @SuppressWarnings("deprecation")
    public TakeTestTest() {
        // This constructor was deprecated - but we want to support lower API levels.
        super(
            "com.ambitioussoftware.k53master",
            MainActivity.class
        );
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        // Espresso will not launch our activity for us, we must launch it via getActivity().
        act = getActivity();
        solo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

    public void testTakeTest() {
        Spoon.screenshot(
            act,
            "Main-Activity-Screenshot"
        );

        onView(withId(R.id.takeTestCard))
            .perform(click());

        onView(withId(R.id.motorcycleCard))
            .check(matches(isDisplayed()));

        Activity act = ActivityHelper.getCurrentActivity(getInstrumentation());
        assertEquals(
            "com.ambitioussoftware.k53master.activity.ChooseVehicleActivity",
            act.getClass().getName()
        );
        Spoon.screenshot(
            act,
            "Choose-Activity-Screenshot"
        );

        onView(withId(R.id.motorcycleCard))
            .perform(click());

        act = ActivityHelper.getCurrentActivity(getInstrumentation());
        assertEquals(
            "com.ambitioussoftware.k53master.activity.TestSetActivity",
            act.getClass().getName()
        );

        Spoon.screenshot(
            act,
            "Test-Set-Screenshot"
        );

        onView(withId(R.id.testSet1))
            .perform(click());

        act = ActivityHelper.getCurrentActivity(getInstrumentation());
        assertEquals(
            "com.ambitioussoftware.k53master.activity.TestActivity",
            act.getClass().getName()
        );

        Spoon.screenshot(
            act,
            "Test-Screenshot"
        );

        // q1
        onView(withId(R.id.answer1))
            .perform(click());
        onView(withId(R.id.next))
            .perform(click());
        // q2
        onView(withId(R.id.answer1))
            .perform(click());
        onView(withId(R.id.next))
            .perform(click());
        // q3
        onView(withId(R.id.answer1))
            .perform(click());
        onView(withId(R.id.next))
            .perform(click());
        // q4
        onView(withId(R.id.answer1))
            .perform(click());
        onView(withId(R.id.next))
            .perform(click());
        // q5
        onView(withId(R.id.answer1))
            .perform(click());
        onView(withId(R.id.next))
            .perform(click());
        // q6
        onView(withId(R.id.answer1))
            .perform(click());
        onView(withId(R.id.next))
            .perform(click());
        // q7
        onView(withId(R.id.answer1))
            .perform(click());
        onView(withId(R.id.next))
            .perform(click());
        // q8
        onView(withId(R.id.answer1))
            .perform(click());
        onView(withId(R.id.next))
            .perform(click());
        // q9
        onView(withId(R.id.answer1))
            .perform(click());
        onView(withId(R.id.next))
            .perform(click());

        onView(withText("Submit"))
            .perform(click());

        act = ActivityHelper.getCurrentActivity(getInstrumentation());
        assertEquals(
            "com.ambitioussoftware.k53master.activity.ResultActivity",
            act.getClass().getName()
        );

        Spoon.screenshot(
            act,
            "Result-Screenshot"
        );
    }
}
