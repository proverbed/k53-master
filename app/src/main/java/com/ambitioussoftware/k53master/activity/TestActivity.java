package com.ambitioussoftware.k53master.activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ambitioussoftware.k53master.data.DatabaseProvider;
import com.ambitioussoftware.k53master.BuildConfig;
import com.ambitioussoftware.k53master.R;
import com.ambitioussoftware.k53master.data.DatabaseHelper;
import com.ambitioussoftware.k53master.model.answerVo;
import com.ambitioussoftware.k53master.util.BitmapWorkerTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestActivity extends ActionBarActivity {

    private static final String TAG = "TestActivity";
    public static final String TEST_ACTIVITY =
        "com.ambitioussoftware.k53master.activity.TestActivity";

    public static final String TOTAL_SIGNS = "com.proverbed.k53master.total.signs";
    public static final String TOTAL_RULES = "com.proverbed.k53master.total.rules";
    public static final String TOTAL_CONTROL = "com.proverbed.k53master.total.control";
    public static final String TOTAL_CORRECT_SIGNS = "com.proverbed.k53master.total.correct.signs";
    public static final String TOTAL_CORRECT_RULES = "com.proverbed.k53master.total.correct.rules";
    public static final String TOTAL_CORRECT_CONTROL = "com.proverbed.k53master.total.correct.control";
    public static final String OUTCOME = "com.proverbed.k53master.outcome";
    public static final String VEHICLE_TYPE = "com.proverbed.k53master.vehicle.type";
    public static final String TEST_SET = "com.proverbed.k53master.test.set";

    public static final String ANSWERED_QUESTIONS = "AnsweredQuestions";
    public static final String ANSWERED_QUESTIONS_ONLY = "AnsweredQuestionsOnly";
    public static final String TIMER = "timer";
    public static final String CURSOR_INDEX_POINTER = "CursorIndexPointer";
    public static final String CURSOR_INDEX = "CursorIndex";
    private int vehicleImage;
    private String testSet;
    private long timer;

    private AlertDialog dialog;

    private Cursor cursor;

    private List<Long> timerReminder = new ArrayList<Long>();

    // @todo release 2 - Update toast reminders with snackbars

    // @todo release 2 - Move scrollbar to top on next button click

    // @todo import actual question/answers

    // @todo release 2 - add timer settings to preferences, analog/dialog

    private int iTotalSigns = 0;
    private int iTotalRules = 0;
    private int iTotalControl = 0;

    private int iTotalSignsCorrect = 0;
    private int iTotalRulesCorrect = 0;
    private int iTotalControlCorrect = 0;

    private int iExistingTotalSigns = 0;
    private int iExistingTotalRules = 0;
    private int iExistingTotalControl = 0;

    /**
     * The minimum correct questions answered for each category
     *
     * Note: These values may differ slightly between testing centres.
     */
    // @todo release 2 - read these values from the preferences
    private static int iMinimumPassSigns = 23;
    private static int iMinimumPassRules = 22;
    private static int iMinimumPassControl = 6;

    // The total number of cursor items
    private int iItems;

    private String iQuestionId;
    private String sQuestion;
    private boolean bestAnsweredCorrect;
    private String sQuestionType;
    private String sDisplayQuestionType;
    private String sAnswer;
    private String sAnswerOption1;
    private String sAnswerOption2;
    private String sAnswerOption3;
    private boolean testOutcome;

    public static final String TESTSET_1 = "1";
    public static final String TESTSET_2 = "2";
    public static final String TESTSET_3 = "3";

    private static final String ANSWER_1 = "1";
    private static final String ANSWER_2 = "2";
    private static final String ANSWER_3 = "3";

    public static final String MOTORCYCLE = "1";
    public static final String LIGHTMOTORVEHICLE = "2";
    public static final String HEAVYMOTORVEHICLE = "3";

    public static final String QTYPERULES = "1";
    public static final String QTYPESIGNS = "2";
    public static final String QTYPECONTROL = "3";

    private Map<String, answerVo> answeredQuestions = new HashMap<String, answerVo>();
    private Map<String, String> answeredQuestionsOnly = new HashMap<String, String>();
    private ArrayList<Integer> cursorIndexes = new ArrayList<Integer>();
    private ArrayList<Integer> cursorRuleIndexes = new ArrayList<Integer>();
    private ArrayList<Integer> cursorSignsIndexes = new ArrayList<Integer>();
    private ArrayList<Integer> cursorControlIndexes = new ArrayList<Integer>();
    private Integer cursorIndexesPointer = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);// Always call the superclass first

        Intent intent = getIntent();
        String vehicleType = intent.getStringExtra(ChooseVehicleActivity.VEHICLE_TYPE);
        testSet = intent.getStringExtra(TestSetActivity.TEST_SET);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "vehicleType: " + vehicleType);
            Log.d(TAG, "testSet: " + testSet);
        }
        String type_id = null;

        switch(vehicleType) {
            case ChooseVehicleActivity.VEHICLE_TYPE_MOTORCYCLE:
                type_id = MOTORCYCLE;
                vehicleImage = R.drawable.motor_cycle;
                break;
            case ChooseVehicleActivity.VEHICLE_TYPE_LIGHTMOTORVEHICLE:
                type_id = LIGHTMOTORVEHICLE;
                vehicleImage = R.drawable.light_motor_vehicle;
                break;
            case ChooseVehicleActivity.VEHICLE_TYPE_HEAVYMOTORVEHICLE:
                type_id = HEAVYMOTORVEHICLE;
                vehicleImage = R.drawable.heavy_motor_vehicle;
                break;
        }

        setContentView(R.layout.test);

        // Add timer reminders - 30 minutes, 10 minutes, and 5 minutes
        timerReminder.add((long)30);
        timerReminder.add((long)10);
        timerReminder.add((long)5);

        cursor = getContentResolver().query(
            DatabaseProvider.getContentUri(
                DatabaseProvider.QUESTION_ANSWER,
                testSet,
                type_id,
                "0"
            ),
            null,
            null,
            null,
            null
        );

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {

            // Restore value of members from saved state
            cursorIndexesPointer = savedInstanceState.getInt(CURSOR_INDEX_POINTER);
            cursorIndexes = savedInstanceState.getIntegerArrayList(CURSOR_INDEX);
            answeredQuestions = (HashMap) savedInstanceState.getSerializable(ANSWERED_QUESTIONS);
            answeredQuestionsOnly = (HashMap) savedInstanceState.getSerializable(ANSWERED_QUESTIONS_ONLY);
            timer = savedInstanceState.getLong(TIMER);
        } else {
            // 1 hour - 3600000
            // 1 minute - 60000
            timer = 3600000;
            cursorIndexesPointer = 0;

            if (cursor.moveToFirst()) {
                do {
                    int position = cursor.getPosition();

                    sQuestionType = cursor.getString(
                        cursor.getColumnIndex(
                            DatabaseHelper.Q_A_COLUMN_QTYPE_ID
                        )
                    );

                    switch(sQuestionType) {
                        case QTYPERULES:
                            cursorRuleIndexes.add(position);
                            break;
                        case QTYPESIGNS:
                            cursorSignsIndexes.add(position);
                            break;
                        case QTYPECONTROL:
                            cursorControlIndexes.add(position);
                            break;
                    }

                    readDataFromDB();
                } while ( cursor.moveToNext() );
            }
            Collections.shuffle(cursorRuleIndexes);
            Collections.shuffle(cursorSignsIndexes);
            Collections.shuffle(cursorControlIndexes);

            if (cursorRuleIndexes.size() != 30 || cursorSignsIndexes.size() != 30 || cursorControlIndexes.size() != 8) {
                String errorMessage = "Database not configured correctly."
                                    + "Rules of the Road questions: [" + cursorRuleIndexes.size() + "]."
                                    + "Signs questions: [" + cursorSignsIndexes.size() + "]."
                                    + "Control questions: [" + cursorControlIndexes.size() + "]."
                                    + "Vehicle Type: [" + vehicleType + "], Test Set: [" + testSet + "]";

                Log.e(TAG, errorMessage);

                throw new RuntimeException(errorMessage);
            }

            cursorIndexes.addAll(cursorRuleIndexes);
            cursorIndexes.addAll(cursorSignsIndexes);
            cursorIndexes.addAll(cursorControlIndexes);
        }

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "timer: " + timer);
        }

        Log.d(TAG, "cursorIndexes: " + cursorIndexes.toString());

        cursor.moveToPosition(cursorIndexes.get(cursorIndexesPointer));

        iItems = cursor.getCount();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Total number of items: " + iItems/3);
        }

        new CountDownTimer(timer, 1000) {

            public void onTick(long millis) {
                timer = millis;

                String output = "";
                long seconds = millis / 1000;
                long minutes = seconds / 60;
                long hours=minutes/ 60;

                seconds = seconds % 60;
                minutes = minutes % 60;
                hours = hours % 60;

                if (timerReminder.contains(minutes) && seconds == 59) {
                    String reminderMessage = "You have " + minutes + " minutes remaining.";
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, reminderMessage);
                    }
                    Toast.makeText(getApplicationContext(), reminderMessage, Toast.LENGTH_LONG).show();
                }

                String secondsD = String.valueOf(seconds);
                String minutesD = String.valueOf(minutes);
                String hoursD = String.valueOf(hours);

                if (seconds < 10) {
                    secondsD = "0" + seconds;
                }
                if (minutes < 10) {
                    minutesD = "0" + minutes;
                }
                if (hours < 10) {
                    hoursD = "0" + hours;
                }
                output = hoursD + ":" + minutesD + ":" + secondsD;

                TextView timer = (TextView) findViewById(R.id.timer);
                timer.setText(output);
            }

            public void onFinish() {

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Sorry your time has expired.");
                    Log.d(TAG, "Cursor position: " + cursor.getPosition());
                    Log.d(TAG, "Cursor items count: " + iItems);
                }

                // move to first because of the start of the question is randomized anywhere in the test set
                if (cursor.moveToFirst()) {
                    Log.d(TAG, "loop");
                    do {
                        Log.d(TAG, "cursor.getPosition(): " + cursor.getPosition());
                        if (cursor.getPosition() != (iItems - 3)) {
                            Log.d(TAG, "if");
                            readDataFromDB();
                            setButtonsVisibility();
                            restoreAnsweredQuestions();
                        } else {
                            Log.d(TAG, "else");
                            readDataFromDB();
                            setButtonsVisibility();
                            restoreAnsweredQuestions();

                            finishTest();
                            Toast.makeText(getApplicationContext(), "Sorry your time has expired.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } while (cursor.moveToNext());
                }
            }
        }.start();

        readDataFromDB();
        setButtonsVisibility();
        updateProgressDetail();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(CURSOR_INDEX_POINTER, cursorIndexesPointer);
        savedInstanceState.putSerializable(ANSWERED_QUESTIONS, (HashMap) answeredQuestions);
        savedInstanceState.putSerializable(ANSWERED_QUESTIONS_ONLY, (HashMap)answeredQuestionsOnly);
        savedInstanceState.putLong(TIMER, timer);
        savedInstanceState.putIntegerArrayList(CURSOR_INDEX, cursorIndexes);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.abort_test_title));
        alertDialog.setMessage(getString(R.string.confirm_abort_test));
        alertDialog.setPositiveButton(
            getString(R.string.abort),
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }
        );
        alertDialog.setNegativeButton(
            getString(R.string.cancel),
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            }
        );
        alertDialog.show();
    }

    private void hideImage(boolean imageVisibility) {
        if (imageVisibility) {
            ((ImageView) findViewById(R.id.testImage)).setVisibility(View.GONE);
        } else {
            ((ImageView) findViewById(R.id.testImage)).setVisibility(View.VISIBLE);
        }
    }

    public void loadBitmap(int resId) {
        BitmapWorkerTask task = new BitmapWorkerTask(
            getApplicationContext(),
            (ImageView) findViewById(R.id.testImage)
        );
        task.execute(resId);
    }

    public void readDataFromDB() {
        iQuestionId = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.QUESTION_COLUMN_ID
            )
        );

        bestAnsweredCorrect = (cursor.getInt(
            cursor.getColumnIndex(
                DatabaseHelper.Q_A_COLUMN_ANSWERED_CORRECT
            )
        ) == 1);

        sQuestionType = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.Q_A_COLUMN_QTYPE_ID
            )
        );

        sQuestion = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.QUESTION_COLUMN_NAME_ALIAS
            )
        );

        String sImage = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.QUESTION_COLUMN_IMAGE_ALIAS
            )
        );

        if (sImage != null) {
            hideImage(false);
            loadBitmap(Integer.parseInt(sImage));
        } else {
            hideImage(true);
        }

        sAnswerOption1 = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.ANSWER_COLUMN_NAME_ALIAS
            )
        );

        if (isCorrectAnswer(cursor)) {
            sAnswer = ANSWER_1;
        }

        if (cursor.moveToNext()) {
            sAnswerOption2 = cursor.getString(
                cursor.getColumnIndex(
                    DatabaseHelper.ANSWER_COLUMN_NAME_ALIAS
                )
            );

            if (isCorrectAnswer(cursor)) {
                sAnswer = ANSWER_2;
            }
        }

        if (cursor.moveToNext()) {
            sAnswerOption3 = cursor.getString(
                cursor.getColumnIndex(
                    DatabaseHelper.ANSWER_COLUMN_NAME_ALIAS
                )
            );

            if (isCorrectAnswer(cursor)) {
                sAnswer = ANSWER_3;
            }
        }

        populateQuestionDetails();
    }

    private boolean isCorrectAnswer(Cursor cursor) {
        int correct = cursor.getInt(
            cursor.getColumnIndex(
                DatabaseHelper.Q_A_COLUMN_CORRECT_ANSWER
            )
        );
        return (correct == 1);
    }

    private void populateQuestionDetails() {
        TextView questionView = (TextView) findViewById(R.id.question);
        questionView.setText(
            sQuestion
        );

        RadioButton answer1View = (RadioButton) findViewById(R.id.answer1);
        answer1View.setText(
            sAnswerOption1
        );

        RadioButton answer2View = (RadioButton) findViewById(R.id.answer2);
        answer2View.setText(
            sAnswerOption2
        );

        RadioButton answer3View = (RadioButton) findViewById(R.id.answer3);
        answer3View.setText(
            sAnswerOption3
        );

        switch(sQuestionType) {
            case QTYPERULES:
                sDisplayQuestionType = getString(R.string.rules);
                break;
            case QTYPESIGNS:
                sDisplayQuestionType = getString(R.string.signs);
                break;
            case QTYPECONTROL:
                sDisplayQuestionType = getString(R.string.controls);
                break;
        }

        TextView questionType = (TextView) findViewById(R.id.questionType);
        questionType.setText(sDisplayQuestionType);
    }

    private void restoreAnsweredQuestions() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioAnswer);
        radioGroup.clearCheck();

        answerVo answer = answeredQuestions.get(iQuestionId);
        if (answer != null) {

            String savedAnswer = answer.selectedAnswer;

            if (savedAnswer != null) {
                switch (savedAnswer) {
                    case ANSWER_1:
                        RadioButton answer1View = (RadioButton) findViewById(R.id.answer1);
                        answer1View.setChecked(true);
                        break;
                    case ANSWER_2:
                        RadioButton answer2View = (RadioButton) findViewById(R.id.answer2);
                        answer2View.setChecked(true);
                        break;
                    case ANSWER_3:
                        RadioButton answer3View = (RadioButton) findViewById(R.id.answer3);
                        answer3View.setChecked(true);
                        break;
                }
            }
        }
    }

    /**
     * Set the button visibility for the prev button, and
     * change the next button to finish, when on the final
     * question.
     */
    private void setButtonsVisibility() {
        Button prevButton = (Button) findViewById(R.id.prev);
        Button nextButton = (Button) findViewById(R.id.next);

        if (cursorIndexesPointer == 0) {
            prevButton.setEnabled(false);
        } else {
            prevButton.setEnabled(true);
        }

        if (cursorIndexesPointer == ((iItems/3)-1)) {
            nextButton.setText(getString(R.string.finishButton));
        } else {
            nextButton.setText(getString(R.string.nextButton));
        }

        // Insert an entry into the hashmap for scoring
        if (answeredQuestions.get(iQuestionId) == null) {
            answerVo answer = new answerVo();
            answer.questionType = sQuestionType;
            answer.bestAnsweredCorrect = bestAnsweredCorrect;

            answeredQuestions.put(iQuestionId, answer);
        }
    }

    public void onAnswerClicked(View view) {
        String answeredQuestion = null;

        switch(view.getId()) {
            case R.id.answer1:
                answeredQuestion = ANSWER_1;
                break;
            case R.id.answer2:
                answeredQuestion = ANSWER_2;
                break;
            case R.id.answer3:
                answeredQuestion = ANSWER_3;
                break;
        }

        answerVo answer = new answerVo();
        answer.selectedAnswer = answeredQuestion;
        answer.questionType = sQuestionType;
        answer.answeredCorrect = sAnswer.equals(answeredQuestion);
        answer.bestAnsweredCorrect = bestAnsweredCorrect;
        
        answeredQuestions.put(iQuestionId, answer);
        answeredQuestionsOnly.put(iQuestionId, null);

        updateProgressDetail();
    }

    private void updateProgressDetail() {
        int count = answeredQuestionsOnly.size();
        int total = (iItems/3);
        String progressDetail = count + " out of " + total + " questions answered";

        TextView progressDetailTextview = (TextView) findViewById(R.id.progressDetail);
        progressDetailTextview.setText(
            progressDetail
        );
        TextView progressPercentTextview = (TextView) findViewById(R.id.progressPercent);
        progressPercentTextview.setText(
            (int)(((double)count/total) * 100) + "% Complete"
        );

        if (BuildConfig.DEBUG) {
            Log.d(TAG, progressDetail);
        }
    }

    /**
     * When the prev button is clicked
     *
     * @param v
     */
    public void onPrevClicked(View v) {
        cursorIndexesPointer--;
        cursor.moveToPosition(cursorIndexes.get(cursorIndexesPointer));

        readDataFromDB();
        setButtonsVisibility();
        restoreAnsweredQuestions();
    }

    /**
     * When the next button is clicked
     *
     * @param v
     */
    public void onNextClicked(View v) {
        // Not the last item
        if (cursorIndexesPointer != ((iItems/3)-1)) {
            cursorIndexesPointer++;
            cursor.moveToPosition(cursorIndexes.get(cursorIndexesPointer));

            readDataFromDB();
            setButtonsVisibility();
            restoreAnsweredQuestions();
        } else {
            String message = getString(R.string.confirm_submit_for_scoring);
            // Check if the user have unanswered questions and update the message accordingly
            if (answeredQuestionsOnly.size() != (iItems/3)) {
                message = getString(R.string.confirm_submit_for_scoring_unanswered);
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage(message)
                .setTitle(getString(R.string.submit_for_scoring_title));
            builder.setNegativeButton(
                getString(R.string.cancel),
                new DialogInterface.OnClickListener() {

                    public void onClick(
                        DialogInterface dialog,
                        int which
                    ) {
                        return;
                    }
                }
            );
            builder.setPositiveButton(
                getString(R.string.submit),
                new DialogInterface.OnClickListener() {
                    public void onClick(
                        DialogInterface dialog,
                        int whichButton
                    ) {

                        finishTest();
                    }
                }
            );
            dialog = builder.create();
            dialog.show();
        }
    }

    private void finishTest() {
        List<String> correctIdList = new ArrayList<String>();
        List<String> incorrectIdList = new ArrayList<String>();

        for (Map.Entry<String, answerVo> entry : answeredQuestions.entrySet()) {
            answerVo answer = entry.getValue();

            switch (answer.questionType) {
                case QTYPERULES:
                    iTotalRules ++;
                    if (answer.answeredCorrect) {
                        iTotalRulesCorrect++;
                        correctIdList.add(entry.getKey());
                    } else {
                        incorrectIdList.add(entry.getKey());
                    }
                    if (answer.bestAnsweredCorrect) {
                        iExistingTotalRules++;
                    }
                    break;
                case QTYPESIGNS:
                    iTotalSigns ++;
                    if (answer.answeredCorrect) {
                        iTotalSignsCorrect++;
                        correctIdList.add(entry.getKey());
                    } else {
                        incorrectIdList.add(entry.getKey());
                    }
                    if (answer.bestAnsweredCorrect) {
                        iExistingTotalSigns++;
                    }
                    break;
                case QTYPECONTROL:
                    iTotalControl ++;
                    if (answer.answeredCorrect) {
                        iTotalControlCorrect++;
                        correctIdList.add(entry.getKey());
                    } else {
                        incorrectIdList.add(entry.getKey());
                    }
                    if (answer.bestAnsweredCorrect) {
                        iExistingTotalControl++;
                    }
                    break;
            }

        }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, iExistingTotalControl + " of " + iTotalControl + " correct existing control");
            Log.d(TAG, iExistingTotalSigns + " of " + iTotalSigns + " correct existing signs");
            Log.d(TAG, iExistingTotalRules + " of " + iTotalRules + " correct existing rules");
        }

        String correctIds = TextUtils.join(",", correctIdList);
        String incorrectIds = TextUtils.join(",", incorrectIdList);

        testOutcome = determineOutcome(iTotalRulesCorrect, iTotalSignsCorrect, iTotalControlCorrect);

        // Save the test score to the database, its its a better result
        if (isBetterResult()) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "This test score is better than the one already saved to"
                    + " the db, and is now being saved as the new best score.");
            }

            // mass update correct entries
            massUpdateAnsweredCorrect(1, correctIds);

            // mass update incorrect entries
            massUpdateAnsweredCorrect(0, incorrectIds);
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "This score is not better than the one already saved " +
                    "to the db");
            }
        }

        Intent intent = new Intent(ResultActivity.RESULT_ACTIVITY);
        intent.putExtra(TOTAL_SIGNS, iTotalSigns);
        intent.putExtra(TOTAL_RULES, iTotalRules);
        intent.putExtra(TOTAL_CONTROL, iTotalControl);
        intent.putExtra(TOTAL_CORRECT_SIGNS, iTotalSignsCorrect);
        intent.putExtra(TOTAL_CORRECT_RULES, iTotalRulesCorrect);
        intent.putExtra(TOTAL_CORRECT_CONTROL, iTotalControlCorrect);
        intent.putExtra(OUTCOME, testOutcome);
        intent.putExtra(VEHICLE_TYPE, vehicleImage);
        intent.putExtra(TEST_SET, Integer.parseInt(testSet));
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    /**
     * Mass update the answered correct status
     *
     * @param answeredCorrect - the answered correct status we want to update to
     * @param ids - comma delimited list of id's we want to update
     */
    private void massUpdateAnsweredCorrect(int answeredCorrect, String ids) {
        if(ids.isEmpty()) {
            return;
        }
        ContentValues values = new ContentValues();
        values.put(
            DatabaseHelper.Q_A_COLUMN_ANSWERED_CORRECT,
            answeredCorrect
        );

        getContentResolver().update(
            DatabaseProvider.getContentUri(
                DatabaseProvider.QUESTION_ANSWER_1,
                ids
            ),
            values,
            null,
            null
        );
    }

    /**
     * Determine if the test scores is a pass or fail.
     *
     * @param iTotalRulesCorrect
     * @param iTotalSignsCorrect
     * @param iTotalControlCorrect
     *
     * @return boolean test outcome
     */
    public static boolean determineOutcome(int iTotalRulesCorrect, int iTotalSignsCorrect, int iTotalControlCorrect) {
        if (
            iTotalRulesCorrect >= iMinimumPassRules &&
            iTotalSignsCorrect >= iMinimumPassSigns &&
            iTotalControlCorrect >= iMinimumPassControl
        ) {
            return true;
        }
        return false;
    }

    /**
     * This function will check if the current test score is better
     * than the test score saved in the database for this test set.
     *
     * @return boolean if this test score is better
     */
    private boolean isBetterResult() {
        int iPassExisting = getNumberPasses(iExistingTotalRules, iExistingTotalSigns, iExistingTotalControl);
        int iPassTestScore = getNumberPasses(iTotalRulesCorrect, iTotalSignsCorrect, iTotalControlCorrect);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "iPassExisting: " + iPassExisting);
            Log.d(TAG, "iPassTestScore: " + iPassTestScore);
        }

        int iCountExistingCorrect = (iExistingTotalRules + iExistingTotalSigns + iExistingTotalControl);
        int iCountTestScoreCorrect = (iTotalRulesCorrect + iTotalSignsCorrect + iTotalControlCorrect);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "iCountExistingCorrect: " + iCountExistingCorrect);
            Log.d(TAG, "iCountTestScoreCorrect: " + iCountTestScoreCorrect);
        }

        if (iPassTestScore > iPassExisting) {
            return true;
        }
        if (iPassTestScore == iPassExisting) {
            if (iCountTestScoreCorrect > iCountExistingCorrect) {
                return true;
            }
            // Special case, when there is no best score, and the best score is 0, then we want to
            // save this to the database, so that it can reflect in the progress screen
            if (iPassTestScore == 0 && iCountTestScoreCorrect == 0 && iCountTestScoreCorrect == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check the number of passes there were for the different question
     * types
     *
     * @param iRulesCorrect
     * @param iSignsCorrect
     * @param iControlCorrect
     *
     * @return int
     */
    private int getNumberPasses(int iRulesCorrect, int iSignsCorrect, int iControlCorrect) {
        int passes = 0;

        if (iRulesCorrect >= iMinimumPassRules) {
            passes++;
        }
        if (iSignsCorrect >= iMinimumPassSigns) {
            passes++;
        }
        if (iControlCorrect >= iMinimumPassControl) {
            passes++;
        }
        return passes;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}