package com.ambitioussoftware.k53master.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ambitioussoftware.k53master.data.DatabaseProvider;
import com.ambitioussoftware.k53master.BuildConfig;
import com.ambitioussoftware.k53master.R;
import com.ambitioussoftware.k53master.data.DatabaseHelper;
import com.ambitioussoftware.k53master.model.answerVo;
import com.ambitioussoftware.k53master.util.BitmapWorkerTask;

import java.util.HashMap;
import java.util.Map;

public class LearnActivity extends ActionBarActivity {

    private static final String TAG = "LearnActivity";

    public static final String LEARN_ACTIVITY =
        "com.ambitioussoftware.k53master.activity.LearnActivity";

    public static final String ANSWERED_QUESTIONS = "AnsweredQuestions";
    public static final String CURSOR_POSITION = "CursorPosition";
    private int cursorPosition;
    private int vehicleImage;
    private String testSet;

    private Cursor cursor;

    // @todo: this serves as a reminder that this learn screen with teach everything, rules, signs, and control questions

    // @todo: maybe in release 2, I can add a screen so the user can choose which section they want
    // to do, rules, signs, or control, but for now, it will just be everything

    // The total number of cursor items
    private int iItems;

    private String iQuestionId;
    private String sQuestion;
    private boolean bestAnsweredCorrect;
    private String sQuestionType;
    private String sDisplayQuestionType;
    private String sAnswer;
    private String sAnswerOption1;
    private String sAnswerOption2;
    private String sAnswerOption3;

    private static final String ANSWER_1 = "1";
    private static final String ANSWER_2 = "2";
    private static final String ANSWER_3 = "3";

    public static final String MOTORCYCLE = "1";
    public static final String LIGHTMOTORVEHICLE = "2";
    public static final String HEAVYMOTORVEHICLE = "3";

    public static final String QTYPERULES = "1";
    public static final String QTYPESIGNS = "2";
    public static final String QTYPECONTROL = "3";

    private Map<String, answerVo> answeredQuestions = new HashMap<String, answerVo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);// Always call the superclass first

        setContentView(R.layout.learn);

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {

            // Restore value of members from saved state
            cursorPosition = savedInstanceState.getInt(CURSOR_POSITION);
            answeredQuestions = (HashMap) savedInstanceState.getSerializable(ANSWERED_QUESTIONS);
        }

        Intent intent = getIntent();
        String vehicleType = intent.getStringExtra(ChooseVehicleActivity.VEHICLE_TYPE);
        testSet = "0"; // we'll make the test set include all test sets for now.

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "vehicleType: " + vehicleType);
            Log.d(TAG, "testSet: " + testSet);
        }

        String type_id = null;

        switch(vehicleType) {
            case ChooseVehicleActivity.VEHICLE_TYPE_MOTORCYCLE:
                type_id = MOTORCYCLE;
                vehicleImage = R.drawable.motor_cycle;
                break;
            case ChooseVehicleActivity.VEHICLE_TYPE_LIGHTMOTORVEHICLE:
                type_id = LIGHTMOTORVEHICLE;
                vehicleImage = R.drawable.light_motor_vehicle;
                break;
            case ChooseVehicleActivity.VEHICLE_TYPE_HEAVYMOTORVEHICLE:
                type_id = HEAVYMOTORVEHICLE;
                vehicleImage = R.drawable.heavy_motor_vehicle;
                break;
        }

        cursor = getContentResolver().query(
            DatabaseProvider.getContentUri(
                DatabaseProvider.QUESTION_ANSWER,
                testSet,
                type_id,
                "0"
            ),
            null,
            null,
            null,
            null
        );

        iItems = cursor.getCount();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Total number of items: " + iItems/3);
        }

        if (savedInstanceState != null) {
            // This minus 2 is necessary since readDataFromDB moves the cursor by 2 forward,
            // this can be fixed in the future to be loosely coupled.
            cursor.moveToPosition(cursorPosition-2);
        } else {
            cursor.moveToFirst();
        }

        readDataFromDB();
        restoreAnsweredQuestions();
        setButtonsVisibility();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(CURSOR_POSITION, cursor.getPosition());
        savedInstanceState.putSerializable(ANSWERED_QUESTIONS, (HashMap) answeredQuestions);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    private void hideImage(boolean imageVisibility) {
        if (imageVisibility) {
            ((ImageView) findViewById(R.id.testImage)).setVisibility(View.GONE);
        } else {
            ((ImageView) findViewById(R.id.testImage)).setVisibility(View.VISIBLE);
        }
    }

    public void loadBitmap(int resId) {
        BitmapWorkerTask task = new BitmapWorkerTask(
            getApplicationContext(),
            (ImageView) findViewById(R.id.testImage)
        );
        task.execute(resId);
    }

    public void readDataFromDB() {
        iQuestionId = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.QUESTION_COLUMN_ID
            )
        );

        bestAnsweredCorrect = (cursor.getInt(
            cursor.getColumnIndex(
                DatabaseHelper.Q_A_COLUMN_ANSWERED_CORRECT
            )
        ) == 1);

        sQuestionType = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.Q_A_COLUMN_QTYPE_ID
            )
        );

        sQuestion = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.QUESTION_COLUMN_NAME_ALIAS
            )
        );

        String sImage = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.QUESTION_COLUMN_IMAGE_ALIAS
            )
        );

        if (sImage != null) {
            hideImage(false);
            loadBitmap(Integer.parseInt(sImage));
        } else {
            hideImage(true);
        }

        sAnswerOption1 = cursor.getString(
            cursor.getColumnIndex(
                DatabaseHelper.ANSWER_COLUMN_NAME_ALIAS
            )
        );

        if (isCorrectAnswer(cursor)) {
            sAnswer = ANSWER_1;

            answerVo answer = new answerVo();
            answer.selectedAnswer = sAnswer;

            answeredQuestions.put( iQuestionId, answer);
        }

        if (cursor.moveToNext()) {
            sAnswerOption2 = cursor.getString(
                cursor.getColumnIndex(
                    DatabaseHelper.ANSWER_COLUMN_NAME_ALIAS
                )
            );

            if (isCorrectAnswer(cursor)) {
                sAnswer = ANSWER_2;

                answerVo answer = new answerVo();
                answer.selectedAnswer = sAnswer;

                answeredQuestions.put( iQuestionId, answer);
            }
        }

        if (cursor.moveToNext()) {
            sAnswerOption3 = cursor.getString(
                cursor.getColumnIndex(
                    DatabaseHelper.ANSWER_COLUMN_NAME_ALIAS
                )
            );

            if (isCorrectAnswer(cursor)) {
                sAnswer = ANSWER_3;

                answerVo answer = new answerVo();
                answer.selectedAnswer = sAnswer;

                answeredQuestions.put( iQuestionId, answer);
            }
        }

        populateQuestionDetails();
    }

    private boolean isCorrectAnswer(Cursor cursor) {
        int correct = cursor.getInt(
            cursor.getColumnIndex(
                DatabaseHelper.Q_A_COLUMN_CORRECT_ANSWER
            )
        );
        return (correct == 1);
    }

    private void populateQuestionDetails() {
        TextView questionView = (TextView) findViewById(R.id.question);
        questionView.setText(
            sQuestion
        );

        RadioButton answer1View = (RadioButton) findViewById(R.id.answer1);
        answer1View.setText(
            sAnswerOption1
        );

        RadioButton answer2View = (RadioButton) findViewById(R.id.answer2);
        answer2View.setText(
            sAnswerOption2
        );

        RadioButton answer3View = (RadioButton) findViewById(R.id.answer3);
        answer3View.setText(
            sAnswerOption3
        );

        switch(sQuestionType) {
            case QTYPERULES:
                sDisplayQuestionType = getString(R.string.rules);
                break;
            case QTYPESIGNS:
                sDisplayQuestionType = getString(R.string.signs);
                break;
            case QTYPECONTROL:
                sDisplayQuestionType = getString(R.string.controls);
                break;
        }

        TextView questionType = (TextView) findViewById(R.id.questionType);
        questionType.setText(sDisplayQuestionType);
    }

    private void restoreAnsweredQuestions() {
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioAnswer);
        radioGroup.clearCheck();

        answerVo answer = answeredQuestions.get(iQuestionId);
        if (answer != null) {

            String savedAnswer = answer.selectedAnswer;

            if (savedAnswer != null) {
                switch (savedAnswer) {
                    case ANSWER_1:
                        RadioButton answer1View = (RadioButton) findViewById(R.id.answer1);
                        answer1View.setChecked(true);
                        break;
                    case ANSWER_2:
                        RadioButton answer2View = (RadioButton) findViewById(R.id.answer2);
                        answer2View.setChecked(true);
                        break;
                    case ANSWER_3:
                        RadioButton answer3View = (RadioButton) findViewById(R.id.answer3);
                        answer3View.setChecked(true);
                        break;
                }
            }
        }
    }

    /**
     * Set the button visibility for the prev button, and
     * change the next button to finish, when on the final
     * question.
     */
    private void setButtonsVisibility() {
        int position = cursor.getPosition();
        Button prevButton = (Button) findViewById(R.id.prev);
        Button nextButton = (Button) findViewById(R.id.next);

        if (position == 2) {
            prevButton.setEnabled(false);
        } else {
            prevButton.setEnabled(true);
        }

        if (position == (iItems-1)) {
            nextButton.setText(getString(R.string.finishButton));
        } else {
            nextButton.setText(getString(R.string.nextButton));
        }

        // Insert an entry into the hashmap for scoring
        if (answeredQuestions.get(iQuestionId) == null) {
            answerVo answer = new answerVo();
            answer.questionType = sQuestionType;
            answer.bestAnsweredCorrect = bestAnsweredCorrect;

            answeredQuestions.put(iQuestionId, answer);
        }
    }

    /**
     * When the prev button is clicked
     *
     * @param v
     */
    public void onPrevClicked(View v) {

        cursor.moveToPosition(cursor.getPosition() - 5);

        readDataFromDB();
        setButtonsVisibility();
        restoreAnsweredQuestions();
    }

    /**
     * When the next button is clicked
     *
     * @param v
     */
    public void onNextClicked(View v) {
        // Not the last item
        if (cursor.getPosition() != (iItems-1)) {
            cursor.moveToNext();

            readDataFromDB();
            setButtonsVisibility();
            restoreAnsweredQuestions();
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}