package com.ambitioussoftware.k53master.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ambitioussoftware.k53master.R;
import com.ambitioussoftware.k53master.data.DatabaseHelper;
import com.ambitioussoftware.k53master.data.DatabaseProvider;
import com.ambitioussoftware.k53master.model.progressItem;

import java.util.ArrayList;
import java.util.List;

public class ProgressActivity extends ActionBarActivity {

    public static final String PROGRESS_ACTIVITY =
        "com.ambitioussoftware.k53master.activity.ProgressActivity";

    private Cursor cursor;
    private boolean testTaken;

    private int iTotalSigns = 0;
    private int iTotalRules = 0;
    private int iTotalControl = 0;

    private int iTotalSignsCorrect = 0;
    private int iTotalRulesCorrect = 0;
    private int iTotalControlCorrect = 0;

    private List<progressItem> progressItemList = new ArrayList<progressItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress);

        // Check if there is any test set scoring data
        cursor = getContentResolver().query(
            DatabaseProvider.getContentUri(
                DatabaseProvider.QUESTION_ANSWER
            ),
            null,
            DatabaseHelper.Q_A_COLUMN_ANSWERED_CORRECT + "= '0'",
            null,
            null
        );

        LinearLayout layout = (LinearLayout)findViewById(R.id.progressContainer);

        // There is no test score results
        if(cursor.getCount() == 0) {
            layout.setVisibility(View.VISIBLE);
        } else {
            layout.setVisibility(View.GONE);

            // @todo: nicetohave in release 1, on this screen have a overall progress bar for each vehicle type that we have test results for

            progressItemList.add(
                new progressItem(
                    TestActivity.MOTORCYCLE,
                    TestActivity.TESTSET_1,
                    R.id.motorCycleTestSet1Card,
                    R.id.motorCycleTestSet1Control,
                    R.id.motorCycleTestSet1Signs,
                    R.id.motorCycleTestSet1Rules,
                    R.id.motorCycleTestSet1Outcome
                )
            );
            progressItemList.add(
                new progressItem(
                    TestActivity.MOTORCYCLE,
                    TestActivity.TESTSET_2,
                    R.id.motorCycleTestSet2Card,
                    R.id.motorCycleTestSet2Control,
                    R.id.motorCycleTestSet2Signs,
                    R.id.motorCycleTestSet2Rules,
                    R.id.motorCycleTestSet2Outcome
                )
            );
            progressItemList.add(
                new progressItem(
                    TestActivity.MOTORCYCLE,
                    TestActivity.TESTSET_3,
                    R.id.motorCycleTestSet3Card,
                    R.id.motorCycleTestSet3Control,
                    R.id.motorCycleTestSet3Signs,
                    R.id.motorCycleTestSet3Rules,
                    R.id.motorCycleTestSet3Outcome
                )
            );
            progressItemList.add(
                new progressItem(
                    TestActivity.LIGHTMOTORVEHICLE,
                    TestActivity.TESTSET_1,
                    R.id.motorCycleTestSet4Card,
                    R.id.motorCycleTestSet4Control,
                    R.id.motorCycleTestSet4Signs,
                    R.id.motorCycleTestSet4Rules,
                    R.id.motorCycleTestSet4Outcome
                )
            );
            progressItemList.add(
                new progressItem(
                    TestActivity.LIGHTMOTORVEHICLE,
                    TestActivity.TESTSET_2,
                    R.id.motorCycleTestSet5Card,
                    R.id.motorCycleTestSet5Control,
                    R.id.motorCycleTestSet5Signs,
                    R.id.motorCycleTestSet5Rules,
                    R.id.motorCycleTestSet5Outcome
                )
            );
            progressItemList.add(
                new progressItem(
                    TestActivity.LIGHTMOTORVEHICLE,
                    TestActivity.TESTSET_3,
                    R.id.motorCycleTestSet6Card,
                    R.id.motorCycleTestSet6Control,
                    R.id.motorCycleTestSet6Signs,
                    R.id.motorCycleTestSet6Rules,
                    R.id.motorCycleTestSet6Outcome
                )
            );
            progressItemList.add(
                new progressItem(
                    TestActivity.HEAVYMOTORVEHICLE,
                    TestActivity.TESTSET_1,
                    R.id.motorCycleTestSet7Card,
                    R.id.motorCycleTestSet7Control,
                    R.id.motorCycleTestSet7Signs,
                    R.id.motorCycleTestSet7Rules,
                    R.id.motorCycleTestSet7Outcome
                )
            );
            progressItemList.add(
                new progressItem(
                    TestActivity.HEAVYMOTORVEHICLE,
                    TestActivity.TESTSET_2,
                    R.id.motorCycleTestSet8Card,
                    R.id.motorCycleTestSet8Control,
                    R.id.motorCycleTestSet8Signs,
                    R.id.motorCycleTestSet8Rules,
                    R.id.motorCycleTestSet8Outcome
                )
            );
            progressItemList.add(
                new progressItem(
                    TestActivity.HEAVYMOTORVEHICLE,
                    TestActivity.TESTSET_3,
                    R.id.motorCycleTestSet9Card,
                    R.id.motorCycleTestSet9Control,
                    R.id.motorCycleTestSet9Signs,
                    R.id.motorCycleTestSet9Rules,
                    R.id.motorCycleTestSet9Outcome
                )
            );

            populateProgressItems();
        }
    }

    /**
     * When clicking take test
     *
     * @param v - the view that was clicked
     */
    public void onTakeTestClicked(View v) {
        Intent intent = new Intent(ChooseVehicleActivity.CHOOSE_VEHICLE_ACTIVITY);
        intent.putExtra(MainActivity.ORIGIN, MainActivity.ORIGIN_TEST);
        startActivity(intent);
    }

    private void populateProgressItems() {
        for(progressItem progressItemEntry : progressItemList){
            getCursorForTypeVersion(progressItemEntry.testSet, progressItemEntry.vehicleType);

            determineCursorTestScore();

            displayResults(
                progressItemEntry.cardId,
                testTaken,
                progressItemEntry.controlId,
                String.format(
                    getString(R.string.control_outcome),
                    iTotalControlCorrect,
                    iTotalControl
                ),
                progressItemEntry.signId,
                String.format(
                    getString(R.string.signs_outcome),
                    iTotalSignsCorrect,
                    iTotalSigns
                ),
                progressItemEntry.ruleId,
                String.format(
                    getString(R.string.rules_outcome),
                    iTotalRulesCorrect,
                    iTotalRules
                )
            );
            displayOutcome(
                progressItemEntry.outcomeId,
                iTotalRulesCorrect, iTotalSignsCorrect, iTotalControlCorrect
            );
        }
    }

    private void displayOutcome(int outcomeViewId, int iTotalRulesCorrect, int iTotalSignsCorrect, int iTotalControlCorrect) {
        TextView outcomeView = (TextView) findViewById(outcomeViewId);
        String outcome = TestActivity.determineOutcome(iTotalRulesCorrect, iTotalSignsCorrect, iTotalControlCorrect) ?
            getString(R.string.pass) : getString(R.string.fail);
        outcomeView.setText(outcome);

        // @todo - fail/pass float right - this is fine for now
    }

    private void getCursorForTypeVersion(String version_id, String type_id) {
        cursor = getContentResolver().query(
            DatabaseProvider.getContentUri(
                DatabaseProvider.QUESTION_ANSWER,
                version_id,
                type_id,
                "1"  // group by question_id flag
            ),
            null,
            null,
            null,
            null
        );
    }

    private void displayResults(
        int cardView, boolean displayCardView,
        int control, String controlText,
        int signs, String signsText,
        int rules, String rulesText) {


        if (displayCardView) {
            TextView rulesView = (TextView) findViewById(control);
            rulesView.setText(controlText);
            TextView signsView = (TextView) findViewById(signs);
            signsView.setText(signsText);
            TextView controlView = (TextView) findViewById(rules);
            controlView.setText(rulesText);

            CardView cardViewDetails = (CardView) findViewById(cardView);
            cardViewDetails.setVisibility(View.VISIBLE);
        }
    }

    private void determineCursorTestScore() {
        iTotalRulesCorrect = 0;
        iTotalSignsCorrect = 0;
        iTotalControlCorrect = 0;

        iTotalRules = 0;
        iTotalSigns = 0;
        iTotalControl = 0;

        testTaken = false;
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            String sQuestionType = cursor.getString(
                cursor.getColumnIndex(
                    DatabaseHelper.Q_A_COLUMN_QTYPE_ID
                )
            );
            boolean answeredCorrect = (cursor.getInt(
                cursor.getColumnIndex(
                    DatabaseHelper.Q_A_COLUMN_ANSWERED_CORRECT
                )
            ) == 1);

            if ((cursor.getInt(
                cursor.getColumnIndex(
                    DatabaseHelper.Q_A_COLUMN_ANSWERED_CORRECT
                )
            ) != 2)) {
                testTaken = true;
            }

            switch (sQuestionType) {
                case TestActivity.QTYPERULES:
                    iTotalRules ++;
                    if (answeredCorrect) {
                        iTotalRulesCorrect++;
                    }
                    break;
                case TestActivity.QTYPESIGNS:
                    iTotalSigns ++;
                    if (answeredCorrect) {
                        iTotalSignsCorrect++;
                    }
                    break;
                case TestActivity.QTYPECONTROL:
                    iTotalControl ++;
                    if (answeredCorrect) {
                        iTotalControlCorrect++;
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
