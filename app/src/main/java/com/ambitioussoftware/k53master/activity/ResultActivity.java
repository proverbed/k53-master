package com.ambitioussoftware.k53master.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ambitioussoftware.k53master.R;

public class ResultActivity extends ActionBarActivity {

    public static final String RESULT_ACTIVITY =
        "com.ambitioussoftware.k53master.activity.ResultActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);

        Intent intent = getIntent();
        int iTotalSigns = intent.getIntExtra(TestActivity.TOTAL_SIGNS, 0);
        int iTotalRules = intent.getIntExtra(TestActivity.TOTAL_RULES, 0);
        int iTotalControl = intent.getIntExtra(TestActivity.TOTAL_CONTROL, 0);
        int iTotalSignsCorrect = intent.getIntExtra(TestActivity.TOTAL_CORRECT_SIGNS, 0);
        int iTotalRulesCorrect = intent.getIntExtra(TestActivity.TOTAL_CORRECT_RULES, 0);
        int iTotalControlCorrect = intent.getIntExtra(TestActivity.TOTAL_CORRECT_CONTROL, 0);
        int iTestSet = intent.getIntExtra(TestActivity.TEST_SET, 0);
        String testSet = null;
        switch(iTestSet) {
            case 1:
                testSet = "Test set A";
                break;
            case 2:
                testSet = "Test set B";
                break;
            case 3:
                testSet = "Test set C";
                break;
        }

        int iVehicleImage = intent.getIntExtra(TestActivity.VEHICLE_TYPE, 0);
        ((ImageView) findViewById(R.id.vehicleType)).setImageResource(iVehicleImage);

        boolean bTestOutcome = intent.getBooleanExtra(TestActivity.OUTCOME, false);

        String sOutcome = getString(R.string.you_fail);
        if (bTestOutcome) {
            sOutcome = getString(R.string.you_pass);
        }

        TextView rulesView = (TextView) findViewById(R.id.rules);
        TextView signsView = (TextView) findViewById(R.id.signs);
        TextView controlView = (TextView) findViewById(R.id.control);
        TextView testOutcomeView = (TextView) findViewById(R.id.testOutcome);
        TextView testSetView = (TextView) findViewById(R.id.testSet);

        rulesView.setText(
            String.format(
                getString(R.string.rules_outcome),
                iTotalRulesCorrect,
                iTotalRules
            )
        );
        signsView.setText(
            String.format(
                getString(R.string.signs_outcome),
                iTotalSignsCorrect,
                iTotalSigns
            )
        );
        controlView.setText(
            String.format(
                getString(R.string.control_outcome),
                iTotalControlCorrect,
                iTotalControl
            )
        );

        testSetView.setText(testSet);
        testOutcomeView.setText(sOutcome);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
