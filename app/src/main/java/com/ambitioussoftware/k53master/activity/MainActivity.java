package com.ambitioussoftware.k53master.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ambitioussoftware.k53master.R;

public class MainActivity extends ActionBarActivity {

    public static final String MAIN_ACTIVITY =
        "com.ambitioussoftware.k53master.activity.MainActivity";

    public static final String ORIGIN = "com.proverbed.k53master.origin";

    public static final String ORIGIN_TEST = "test";

    public static final String ORIGIN_LEARN = "learn";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // @todo release 2 nicetohave: add a progress bar, for test sets in the progress card view. progress as a whole on the dashboard

        // @todo add about screen in menu overflow

        // @todo styles the application for multiple devices
    }

    /**
     * When clicking learn
     *
     * @param v - the view that was clicked
     */
    public void onLearnClicked(View v) {
        Intent intent = new Intent(ChooseVehicleActivity.CHOOSE_VEHICLE_ACTIVITY);
        intent.putExtra(ORIGIN, ORIGIN_LEARN);
        startActivity(intent);
    }

    /**
     * When clicking take test
     *
     * @param v - the view that was clicked
     */
    public void onTakeTestClicked(View v) {
        Intent intent = new Intent(ChooseVehicleActivity.CHOOSE_VEHICLE_ACTIVITY);
        intent.putExtra(ORIGIN, ORIGIN_TEST);
        startActivity(intent);
    }

    /**
     * When clicking the progress card on the main activity
     *
     * @param v - the view that was clicked
     */
    public void onProgressClicked(View v) {
        Intent intent = new Intent(ProgressActivity.PROGRESS_ACTIVITY);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
