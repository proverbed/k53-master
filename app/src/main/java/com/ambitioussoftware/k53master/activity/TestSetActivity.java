package com.ambitioussoftware.k53master.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ambitioussoftware.k53master.R;

public class TestSetActivity extends ActionBarActivity {

    public static final String TESTSET_ACTIVITY =
        "com.ambitioussoftware.k53master.activity.TestSetActivity";

    public static final String TEST_SET = "com.proverbed.k53master.test.set";
    public static final String TEST_SET_1 = "1";
    public static final String TEST_SET_2 = "2";
    public static final String TEST_SET_3 = "3";

    private String vehicleType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_set);

        Intent intent = getIntent();
        vehicleType = intent.getStringExtra(ChooseVehicleActivity.VEHICLE_TYPE);

    }

    /**
     * When selecting test set 1
     *
     * @param v - the view that was clicked
     */
    public void onTestSet1Clicked(View v) {
        Intent intent = new Intent(TestActivity.TEST_ACTIVITY);
        intent.putExtra(ChooseVehicleActivity.VEHICLE_TYPE, vehicleType);
        intent.putExtra(TEST_SET, TEST_SET_1);
        startActivity(intent);
    }

    /**
     * When selecting test set 2
     *
     * @param v - the view that was clicked
     */
    public void onTestSet2Clicked(View v) {
        Intent intent = new Intent(TestActivity.TEST_ACTIVITY);
        intent.putExtra(ChooseVehicleActivity.VEHICLE_TYPE, vehicleType);
        intent.putExtra(TEST_SET, TEST_SET_2);
        startActivity(intent);
    }

    /**
     * When selecting test set 3
     *
     * @param v - the view that was clicked
     */
    public void onTestSet3Clicked(View v) {
        Intent intent = new Intent(TestActivity.TEST_ACTIVITY);
        intent.putExtra(ChooseVehicleActivity.VEHICLE_TYPE, vehicleType);
        intent.putExtra(TEST_SET, TEST_SET_3);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
