package com.ambitioussoftware.k53master.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ambitioussoftware.k53master.BuildConfig;
import com.ambitioussoftware.k53master.R;

public class ChooseVehicleActivity extends ActionBarActivity {

    private String action;
    private static final String TAG = "ChooseVehicleActivity";
    public static final String CHOOSE_VEHICLE_ACTIVITY =
        "com.ambitioussoftware.k53master.activity.ChooseVehicleActivity";

    public static final String VEHICLE_TYPE = "com.proverbed.k53master.vehicle.type";
    public static final String VEHICLE_TYPE_MOTORCYCLE = "motorcycle";
    public static final String VEHICLE_TYPE_LIGHTMOTORVEHICLE = "lightmotorvehicle";
    public static final String VEHICLE_TYPE_HEAVYMOTORVEHICLE = "heavymotorvehicle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_vehicle);

        Intent intent = getIntent();
        String origin = intent.getStringExtra(MainActivity.ORIGIN);

        switch(origin) {
            case MainActivity.ORIGIN_TEST:
                action = TestSetActivity.TESTSET_ACTIVITY;
                break;
            case MainActivity.ORIGIN_LEARN:
                action = LearnActivity.LEARN_ACTIVITY;
                break;
            default:
                throw new RuntimeException("Unknown origin [" + origin + "] given.");
        }

        if (BuildConfig.DEBUG) {

            DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();

            float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
            float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

            Log.d(TAG, "height dp: " + dpHeight);
            Log.d(TAG, "width dp: " + dpWidth);
        }
    }

    /**
     * When motor cycle is clicked
     *
     * @param v - the view that was clicked
     */
    public void onMotorCycleClicked(View v) {
        Intent intent = new Intent(action);
        intent.putExtra(VEHICLE_TYPE, VEHICLE_TYPE_MOTORCYCLE);
        startActivity(intent);
    }

    /**
     * When light motor vehicle is clicked
     *
     * @param v - the view that was clicked
     */
    public void onLightMotorVehicleClicked(View v) {
        Intent intent = new Intent(action);
        intent.putExtra(VEHICLE_TYPE, VEHICLE_TYPE_LIGHTMOTORVEHICLE);
        startActivity(intent);
    }

    /**
     * When heavy motor vehicle is clicked
     *
     * @param v - the view that was clicked
     */
    public void onHeavyMotorVehicleClicked(View v) {
        Intent intent = new Intent(action);
        intent.putExtra(VEHICLE_TYPE, VEHICLE_TYPE_HEAVYMOTORVEHICLE);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
