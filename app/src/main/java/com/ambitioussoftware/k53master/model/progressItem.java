package com.ambitioussoftware.k53master.model;

/**
 * Project item object class
 */
public class progressItem {

    public String vehicleType;

    public String testSet;

    public int cardId;

    public int controlId;

    public int signId;

    public int ruleId;

    public int outcomeId;

    public progressItem(){

    }

    public progressItem(
        String vehicleType,
        String testSet,
        int cardId,
        int controlId,
        int signId,
        int ruleId,
        int outcomeId
    ) {
        this.vehicleType = vehicleType;
        this.testSet = testSet;
        this.cardId = cardId;
        this.controlId = controlId;
        this.signId = signId;
        this.ruleId = ruleId;
        this.outcomeId = outcomeId;
    }
}
