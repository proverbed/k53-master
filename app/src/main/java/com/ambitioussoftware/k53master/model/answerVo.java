package com.ambitioussoftware.k53master.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Create answer variable object class
 */
public class answerVo implements Parcelable {
    public String selectedAnswer;
    public String questionType;
    public boolean answeredCorrect;

    /**
     * Whether this answer was correct in the best test score
     */
    public boolean bestAnsweredCorrect;

    @Override
    public boolean equals(Object o) {
        if (o instanceof answerVo) {
            answerVo d = (answerVo)o;
            return d.selectedAnswer.equals( selectedAnswer ) &&
                d.questionType.equals( questionType ) &&
                (d.answeredCorrect == answeredCorrect);
        } else {
            return false;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(selectedAnswer);
        dest.writeString(questionType);
        dest.writeByte((byte) (answeredCorrect ? 1 : 0));
    }
}
