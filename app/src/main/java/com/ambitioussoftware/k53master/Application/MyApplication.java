package com.ambitioussoftware.k53master.Application;

import android.app.Application;

import org.acra.*;
import org.acra.annotation.*;

@ReportsCrashes(
    formUri = "http://ambitioussoftware.iriscouch.com/acra-k53master/_design/acra-storage/_update/report",
    reportType = org.acra.sender.HttpSender.Type.JSON,
    httpMethod = org.acra.sender.HttpSender.Method.PUT,
    formUriBasicAuthLogin="proverbedReporter",
    formUriBasicAuthPassword="j@4*Ik3Kjdl"
    // Your usual ACRA configuration
)
public class MyApplication extends Application {
    @Override
    public final void onCreate() {
        super.onCreate();
        ACRA.init(this);
    }
}
