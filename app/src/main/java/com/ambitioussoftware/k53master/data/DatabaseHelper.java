package com.ambitioussoftware.k53master.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ambitioussoftware.k53master.BuildConfig;
import com.ambitioussoftware.k53master.R;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";
    private SQLiteDatabase database;

    private static DatabaseHelper sInstance;

    public static final String QUESTION_TABLE_NAME = "question";
    public static final String QUESTION_COLUMN_ID = "_id";
    public static final String QUESTION_COLUMN_NAME = "name";
    public static final String QUESTION_COLUMN_IMAGE = "image";
    public static final String QUESTION_COLUMN_NAME_ALIAS = "question_name";
    public static final String QUESTION_COLUMN_IMAGE_ALIAS = "question_image";

    public static final String ANSWER_TABLE_NAME = "answer";
    public static final String ANSWER_COLUMN_ID = "_id";
    public static final String ANSWER_COLUMN_NAME = "name";
    public static final String ANSWER_COLUMN_NAME_ALIAS = "answer_name";

    public static final String Q_A_TABLE_NAME = "question_answer";
    public static final String Q_A_COLUMN_ID = "_id";
    public static final String Q_A_COLUMN_QUESTION_ID = "question_id";
    public static final String Q_A_COLUMN_ANSWER_ID = "answer_id";
    public static final String Q_A_COLUMN_VERSION_ID = "version_id";
    public static final String Q_A_COLUMN_TYPE_ID = "type_id";
    public static final String Q_A_COLUMN_QTYPE_ID = "qtype_id";
    public static final String Q_A_COLUMN_CORRECT_ANSWER = "bCorrectAnswer";
    public static final String Q_A_COLUMN_ANSWERED_CORRECT = "bAnsweredCorrect";

    public static final String DATABASE_NAME = "k53Learners.db";
    private static final int DATABASE_VERSION = 3;

    private int MOTORCYCLE = 1;
    private int LIGHTMOTORVEHICLE = 2;
    private int HEAVYMOTORVEHICLE = 3;

    private int SETA = 1;
    private int SETB = 2;
    private int SETC = 3;

    private int QTYPERULES = 1;
    private int QTYPESIGNS = 2;
    private int QTYPECONTROL = 3;

    private Context context;

    // Answer table creation sql statement
    private static final String ANSWER_DATABASE_TABLE = " CREATE TABLE "
        + ANSWER_TABLE_NAME + "(" + ANSWER_COLUMN_ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, " + ANSWER_COLUMN_NAME
        + " TEXT NOT NULL);";

    // Question table creation sql statement
    private static final String QUESTION_DATABASE_TABLE = " CREATE TABLE "
        + QUESTION_TABLE_NAME + "(" + QUESTION_COLUMN_ID
        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + QUESTION_COLUMN_NAME + " TEXT NOT NULL, "
        + QUESTION_COLUMN_IMAGE + " TEXT DEFAULT NULL"
        + ");";

    // Question & Answer table creation sql statement
    private static final String Q_A_DATABASE_TABLE = " CREATE TABLE " + Q_A_TABLE_NAME + "("
        + Q_A_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
        + Q_A_COLUMN_QUESTION_ID + " INTEGER NOT NULL, "
        + Q_A_COLUMN_ANSWER_ID + " INTEGER NOT NULL, "
        + Q_A_COLUMN_VERSION_ID + " INTEGER NOT NULL, "
        + Q_A_COLUMN_TYPE_ID + " INTEGER NOT NULL, "
        + Q_A_COLUMN_QTYPE_ID + " INTEGER NOT NULL, "
        + Q_A_COLUMN_CORRECT_ANSWER + " INTEGER DEFAULT 0, "
        + Q_A_COLUMN_ANSWERED_CORRECT + " INTEGER DEFAULT 2, "
        + "FOREIGN KEY( " + Q_A_COLUMN_QUESTION_ID + " ) REFERENCES "
        + QUESTION_TABLE_NAME + "(" + QUESTION_COLUMN_ID + "),"
        + "FOREIGN KEY( " + Q_A_COLUMN_ANSWER_ID + " ) REFERENCES "
        + ANSWER_TABLE_NAME + "(" + ANSWER_COLUMN_ID + ")" + ");";

    private DatabaseHelper(Context context) {
        super(
            context,
            DATABASE_NAME,
            null,
            DATABASE_VERSION
        );
        this.context = context;
    }

    public static DatabaseHelper getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        try {
            this.database = database;

            database.execSQL(ANSWER_DATABASE_TABLE);
            database.execSQL(QUESTION_DATABASE_TABLE);
            database.execSQL(Q_A_DATABASE_TABLE);

            insertDefaultData();
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.d(
                    TAG,
                    "onCreate: Something went wrong, when adding default data." + e.toString()
                );
            }
        }
    }

    /**
     * Insert default data
     *
     * This will be the question and answer combinations
     */
    private void insertDefaultData() {
        long question_id;
        int[] allVehicles = new int[]{MOTORCYCLE, LIGHTMOTORVEHICLE, HEAVYMOTORVEHICLE};
        int[] lightAndHeavyMotorvehicle = new int[]{LIGHTMOTORVEHICLE, HEAVYMOTORVEHICLE};
        int[] onlyMotorcycle = new int[]{MOTORCYCLE};
        int[] onlyLightMotorvehicle = new int[]{LIGHTMOTORVEHICLE};
        int[] onlyHeavyMotorvehicle = new int[]{HEAVYMOTORVEHICLE};
        int[] onlySetA = new int[]{SETA};
        int[] onlySetB = new int[]{SETB};
        int[] onlySetC = new int[]{SETC};
        int[] setAB = new int[]{SETA, SETB};
        int[] setAC = new int[]{SETA, SETC};
        int[] setABC = new int[]{SETA, SETB, SETC};
        int[] setBC = new int[]{SETB, SETC};

        /**
         * LIGHT MOTOR VEHICLE
         * HEAVY MOTOR VEHICLE
         * MOTORCYCLE
         *
         * Set A - 1 CONTROL
         * Set B - 1 CONTROL
         * 1 QUESTIONS
         */
        question_id = insertQuestion("The distance it takes you to stop your vehicle is…\n" +
            "\n" +
            "(i) Longer on a wet road than on a dry road.\n" +
            "(ii) Longer if you ride a higher speed.\n" +
            "(iii) Longer if it is loaded.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "None of the above are correct", QTYPECONTROL, allVehicles, setABC);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPECONTROL, allVehicles, setABC, true);
        insertQuestionAnswer(question_id, "(i) Only is correct", QTYPECONTROL, allVehicles, setABC);

        /**
         * LIGHT MOTOR VEHICLE
         * HEAVY MOTOR VEHICLE
         * MOTORCYCLE
         *
         * Set A - 26 RULES
         * Set B - 12 RULES
         */
        question_id = insertQuestion("Which rule is considered the most important RULE OF THE ROAD in South Africa?");
        insertQuestionAnswer(question_id, "Always be courteous and considerate towards fellow road users.", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "Do not exceed the speed limit.", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "Keep to the left side of the road far as is safe.", QTYPERULES, allVehicles, setABC, true);

        question_id = insertQuestion("Under what circumstances, if any, are you allowed to drive your vehicle on the right-hand side of a public road with traffic moving in both directions?");
        insertQuestionAnswer(question_id, "When you switch the emergency lights of your vehicle on.", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "When a traffic officer order you to do so.", QTYPERULES, allVehicles, setABC, true);
        insertQuestionAnswer(question_id, "Under no circumstance.", QTYPERULES, allVehicles, setABC);

        question_id = insertQuestion("You may not obtain a learner’s license if…\n" +
            "\n" +
            "(i) You already have a license that authorizes the driving of the same vehicle class.\n" +
            "(ii) You are declared unfit to obtain a driving license for a certain period and that period still prevails.\n" +
            "(iii) Your license was suspended temporarily and the suspension has not expired.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "(i) Only is correct", QTYPERULES, allVehicles, setABC, true);
        insertQuestionAnswer(question_id, "(ii) And (iii) only are correct", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "(i), (ii) and (iii) are correct", QTYPERULES, allVehicles, setABC);

        question_id = insertQuestion("A vehicle of which the brakes are not good, must be towed…");
        insertQuestionAnswer(question_id, "With a rope.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "With a tow-bar.", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "On a trailer.", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("A safe following distance is, when the vehicle in front of you suddenly stops, you could…");
        insertQuestionAnswer(question_id, "Stop without swerving.", QTYPERULES, allVehicles, setAB, true);
        insertQuestionAnswer(question_id, "Swerve and stop next to it.", QTYPERULES, allVehicles, setAB);
        insertQuestionAnswer(question_id, "Swerve and pass.", QTYPERULES, allVehicles, setAB);

        question_id = insertQuestion("Unless otherwise shown by a sign, the general speed limit in a town or city is… km/h.");
        insertQuestionAnswer(question_id, "60", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "80", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "100", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("The furthest that your vehicle’s dim lights may shine in front of you, is…m.");
        insertQuestionAnswer(question_id, "45", QTYPERULES, allVehicles, setAB, true);
        insertQuestionAnswer(question_id, "100", QTYPERULES, allVehicles, setAB);
        insertQuestionAnswer(question_id, "150", QTYPERULES, allVehicles, setAB);

        question_id = insertQuestion("You may not…");
        insertQuestionAnswer(question_id, "Have passengers in the vehicle if you have a learner’s license.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Leave your vehicle unattended while the engine is running.", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Drive in reverse for more than 100m.", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("The last action that you must take before moving to another lane is to…");
        insertQuestionAnswer(question_id, "Switch on your indicator.", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "Check the blind spot.", QTYPERULES, allVehicles, setABC, true);
        insertQuestionAnswer(question_id, "Look in the rear view mirror.", QTYPERULES, allVehicles, setABC);

        question_id = insertQuestion("If you want to change lanes, you must…");
        insertQuestionAnswer(question_id, "Switch on your indicator and change lanes.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Give the necessary signal and after looking for other traffic, change lanes.", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Apply the brakes lightly and then change lanes.", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("You may pass another vehicle on its left-hand side if it…" +
            "\n\n" +
            "(i) Indicates that it is going to turn right.\n" +
            "(ii) Drives on the right-hand side of a road with a shoulder where you can pass.\n" +
            "(iii) Drives in a town in the right-hand lane with two lanes in the same direction.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "(i) and (iii) only are correct", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "(i) Only is correct", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("You are not allowed to stop…");
        insertQuestionAnswer(question_id, "Opposite a vehicle, where the roadway is 10 m wide.", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "Where you are also not allowed to park.", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "5 m from bridge.", QTYPERULES, allVehicles, setABC, true);

        question_id = insertQuestion("You may on public road…");
        insertQuestionAnswer(question_id, "Pass another vehicle turning right, on the left-hand side without driving on the shoulder of the road.", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Pass another vehicle at any place on the left-hand side if it is turning right.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Not pass any vehicle on the left-hand side.", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("At an intersection…");
        insertQuestionAnswer(question_id, "Pedestrians, who are already crossing the road when the red man signal starts showing, have the right of way.", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "You can pass another vehicle waiting to turn right on its left side by going off the road.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "You can stop in it to off load passengers.", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("The fastest speed at which you may tow a vehicle with rope is… km/h.");
        insertQuestionAnswer(question_id, "60", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "45", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "30", QTYPERULES, allVehicles, onlySetA, true);

        question_id = insertQuestion("You may cross or enter a public road…");
        insertQuestionAnswer(question_id, "If the road is clear of traffic for a short distance.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "If the road is clear of traffic for along distance and can be done without obstructing traffic.", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "In any manner as long you use your indicator in time.", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("Your vehicle’s headlights must be switched on…" +
            "\n\n" +
            "(i) At any time of the day when you cannot see persons and vehicles 150 m in front of you.\n" +
            "(ii) From sunset to sunrise.\n" +
            "(iii) When it rains and you cannot see vehicles 100 m in front of you.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION\n");
        insertQuestionAnswer(question_id, "(ii) Only is correct", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "(i) and (ii) only are correct", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, allVehicles, setABC, true);

        question_id = insertQuestion("When may you NOT pass another vehicle? When you…\n" +
            "\n" +
            "(i) Are nearing the top of a hill.\n" +
            "(ii) Are nearing a curve.\n" +
            "(iii) Can only see 100 m in front of you because of smoke over the road.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION\n");
        insertQuestionAnswer(question_id, "(i) Only is correct", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, allVehicles, setABC, true);
        insertQuestionAnswer(question_id, "(i) and (ii) only are correct", QTYPERULES, allVehicles, setABC);

        question_id = insertQuestion("You may Not drive into an intersection when…");
        insertQuestionAnswer(question_id, "The robot (traffic signal) is yellow and you are already in the intersection.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "The vehicle in front of you wants to turn right and the road is wide enough to pass on the left-hand side.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "There is not enough space in the intersection to turn right without blocking other traffic.", QTYPERULES, allVehicles, onlySetA, true);

        question_id = insertQuestion("When the robot is red and the green arrow flashes to the right as indicated, it shows you that…", R.drawable.x5);
        insertQuestionAnswer(question_id, "Only pedestrians may walk.", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "If you want to turn right, you may go.", QTYPERULES, allVehicles, setABC, true);
        insertQuestionAnswer(question_id, "All traffic must turn right there.", QTYPERULES, allVehicles, setABC);

        question_id = insertQuestion("When you were involved in an accident you…\n" +
            "\n" +
            "(i) Must immediately stop your vehicle.\n" +
            "(ii) Must see if somebody is injured.\n" +
            "(iii) May use a little bit of alcohol for the shock.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "(i) and (ii) only are correct", QTYPERULES, allVehicles, setAC, true);
        insertQuestionAnswer(question_id, "(ii) Only is correct", QTYPERULES, allVehicles, setAC);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, allVehicles, setAC);

        question_id = insertQuestion("If you come to a robot and the red light flashes, you must…");
        insertQuestionAnswer(question_id, "Stop and wait for the light to turn green before you go.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Stop and go only if it is safe to do so.", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Look out for a roadblock as the light shows you a police stop.", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("The legal speed, which you may drive…");
        insertQuestionAnswer(question_id, "Is always 120 km/h outside towns/cities.", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "Can be determined by yourself if you look at the number of lanes the road has.", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "Is shown to you by signs next to the road.", QTYPERULES, allVehicles, setABC, true);

        question_id = insertQuestion("When do you have the right of way?\n" +
            "\n" +
            "(i) When you are within a traffic circle.\n" +
            "(ii) When you have stopped first at a four-way stop.\n" +
            "(iii) When you want to turn right at an intersection in two-way road.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "(i) and (ii) only are correct", QTYPERULES, allVehicles, setABC, true);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, allVehicles, setABC);
        insertQuestionAnswer(question_id, "(i) only is correct", QTYPERULES, allVehicles, setABC);

        question_id = insertQuestion("If you come across an emergency vehicle on the sounding a siren, you must");
        insertQuestionAnswer(question_id, "Flash your headlights to warn other traffic", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Give right of way to the emergency vehicle.", QTYPERULES, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Switch on your vehicles emergency lights and blow your hooter.", QTYPERULES, allVehicles, onlySetA);

        question_id = insertQuestion("If you want to turn left with your vehicle, you must …");
        insertQuestionAnswer(question_id, "Slow down completely, stop and then turn.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "First move to the right to enable him to turn left easily.", QTYPERULES, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Give the necessary signal in good time.", QTYPERULES, allVehicles, onlySetA, true);

        /**
         * LIGHT MOTOR VEHICLE
         * HEAVY MOTOR VEHICLE
         * MOTORCYCLE
         *
         * Set A - 26 SIGNS
         * Set B - 3 SIGNS
         */
        question_id = insertQuestion("The following sign warns you about… ahead.", R.drawable.l9);
        insertQuestionAnswer(question_id, "A first-aid post", QTYPESIGNS, allVehicles, setABC);
        insertQuestionAnswer(question_id, "A railway crossing", QTYPESIGNS, allVehicles, setABC);
        insertQuestionAnswer(question_id, "Road which cross", QTYPESIGNS, allVehicles, setABC, true);

        question_id = insertQuestion("The following sign is found at a…", R.drawable.cc9);
        insertQuestionAnswer(question_id, "Traffic circle where right of way is applicable.", QTYPESIGNS, allVehicles, setABC, true);
        insertQuestionAnswer(question_id, "Dangerous place where roadwork is being done.", QTYPESIGNS, allVehicles, setABC);
        insertQuestionAnswer(question_id, "Sharp curve to the right.", QTYPESIGNS, allVehicles, setABC);

        question_id = insertQuestion("What does The following warning sign show you?", R.drawable.l10);
        insertQuestionAnswer(question_id, "Roads cross ahead and other vehicle from the side must stop or yield at the intersection.", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "A railway line intersects with road that you are traveling on.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "A 4-way-stop will be found ahead where the roads cross.", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following sign shows you that you…", R.drawable.b6);
        insertQuestionAnswer(question_id, "Must only drive straight on.", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Will get a freeway ahead.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Will get a one-way road ahead.", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following sign...", R.drawable.e1);
        insertQuestionAnswer(question_id, "Shows you that there is a hospital ahead where you must not make noise.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Shows you that you may not pick up people.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Prohibits you from using your hooter.", QTYPESIGNS, allVehicles, onlySetA, true);

        question_id = insertQuestion("The following sign shows you that…", R.drawable.p2);
        insertQuestionAnswer(question_id, "It is a special lane for cyclists.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "No cyclists are allowed there.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "You must be on the lookout for cyclists.", QTYPESIGNS, allVehicles, onlySetA, true);

        question_id = insertQuestion("What does the following sign show you?\n" +
            "\n" +
            "(i) You may not turn left ahead.\n" +
            "(ii) You may not turn left there.\n" +
            "(iii) There is a one-way to the right ahead.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION", R.drawable.e4);
        insertQuestionAnswer(question_id, "(i) Only is correct", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "(ii) and (iii) only are correct", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "(i) and (iii) only are correct", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following road marking warns you that…", R.drawable.z5);
        insertQuestionAnswer(question_id, "There is a level crossing with one railway line ahead.", QTYPESIGNS, allVehicles, setAB, true);
        insertQuestionAnswer(question_id, "Roads cross ahead.", QTYPESIGNS, allVehicles, setAB);
        insertQuestionAnswer(question_id, "The freeway ends ahead.", QTYPESIGNS, allVehicles, setAB);

        question_id = insertQuestion("The following sign shows you that the road…", R.drawable.r1);
        insertQuestionAnswer(question_id, "Winds", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Is untarred", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Is slippery.", QTYPESIGNS, allVehicles, onlySetA, true);

        question_id = insertQuestion("What does the following sign show you?", R.drawable.a3);
        insertQuestionAnswer(question_id, "It is the same as a yield sign and you can proceed without stopping.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Make sure that it is safe, wait until all traffic has departed and then drive on.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Stop and drive on when it is safe to do so in the sequence that the vehicle stopped at the stop line.", QTYPESIGNS, allVehicles, onlySetA, true);

        question_id = insertQuestion("The following sign shows you that the road…", R.drawable.u4);
        insertQuestionAnswer(question_id, "Winds for 12 km.", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Winds 12 km form there.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Is slippery for 12 km.", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following sign shows that…", R.drawable.e3);
        insertQuestionAnswer(question_id, "There is a road with two lanes ahead.", QTYPESIGNS, allVehicles, setAC);
        insertQuestionAnswer(question_id, "You are not allowed to drive there without permission.", QTYPESIGNS, allVehicles, setAC, true);
        insertQuestionAnswer(question_id, "A freeway starts.", QTYPESIGNS, allVehicles, setAC);

        question_id = insertQuestion("What does the following sign show you?", R.drawable.b1);
        insertQuestionAnswer(question_id, "That sign is applicable for 50 m.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "You may not drive faster than 50 km/h.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "You may not drive slower than 50 km/h.", QTYPESIGNS, allVehicles, onlySetA, true);

        question_id = insertQuestion("The following sign shows you the…", R.drawable.j8);
        insertQuestionAnswer(question_id, "Maximum speed allowed at night.", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Recommended speed when your lights are on.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Distance to the next town.", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following sign shows you that…", R.drawable.e9);
        insertQuestionAnswer(question_id, "Only motorcars may not pass each other.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "No motor vehicle may pass each other.", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "There is a bridge ahead where only one vehicle at a time can cross.", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following road marking in the sketch…", R.drawable.gm1);
        insertQuestionAnswer(question_id, "Shows the lanes for road users.", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Divides the road in two sections.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Is only found in a parking area.", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following road marking in the sketch, shows you that you…", R.drawable.rtm1);
        insertQuestionAnswer(question_id, "If you are driving in car A, must stop before this line.", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "If you are driving in car A, must reduce speed and drive on if is safe.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "If you are driving in car B, can pass car A if there are no other vehicles in the intersection.", QTYPESIGNS, allVehicles, onlySetA);

        //What is the difference between road marking AA6 and road marking Y3?
        question_id = insertQuestion("What is the difference between road marking AA6 and road marking Y3?", R.drawable.aa6_y3);
        insertQuestionAnswer(question_id, "There is no difference between the two and both shows you that you may not pass other vehicles.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Both show you that you may not pass other vehicles but you will find AA6 on all roads and Y3 on freeways only.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "AA6 shows you that you may not pass other vehicles while Y3 as well shows you that you may not drive over it to turn into a yard or anything else.", QTYPESIGNS, allVehicles, onlySetA, true);

        question_id = insertQuestion("When are you allowed to drive in the section left of the yellow line, RM4.1 in the sketch.\n" +
            "\n" +
            "(i) Any time when you want to allow another vehicle to pass you.\n" +
            "(ii) In daytime when you want to allow another vehicle to pass you.\n" +
            "(iii) When on a freeway with 2 lanes in both directions, you want to drive slower than 120 km/h.\n" +
            "(iv) When you have a flat tyre and you want to park there to change it.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION", R.drawable.rm4_1);
        insertQuestionAnswer(question_id, "(i), (iii) and (iv) only are correct", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "(ii) and (iv) only are correct", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("All road signs with a yellow background are… signs.", R.drawable.temporary);
        insertQuestionAnswer(question_id, "Warning", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Temporary", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Tourism", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following marking shows that only … may park there.", R.drawable.bb3);
        insertQuestionAnswer(question_id, "Buses", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "Business vehicles", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Minibuses", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following sign shows you that…", R.drawable.a8);
        insertQuestionAnswer(question_id, "The road ends.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "You are not allowed to enter there.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "The entry is for ambulances only.", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("When the following sign is illuminated it shows you that…", R.drawable.x8);
        insertQuestionAnswer(question_id, "There is no throughway there", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "There is an unguarded railway crossing ahead.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "It is a lane only for traffic coming from the front.", QTYPESIGNS, allVehicles, onlySetA, true);

        question_id = insertQuestion("The following road marking show you that…", R.drawable.aa4);
        insertQuestionAnswer(question_id, "You are not allowed to pass another vehicle there.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "A line is going to begin which prohibits you from passing.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "The lane splits into more lanes in the direction of the arrow.", QTYPESIGNS, allVehicles, onlySetA);

        question_id = insertQuestion("The following sign prohibits you from…", R.drawable.l4);
        insertQuestionAnswer(question_id, "Using that section of the road during certain hours.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Parking there between 09:00 and 16:00.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "Stopping there at certain hours.", QTYPESIGNS, allVehicles, onlySetA, true);

        //What does sign S1 when it flashes show you?
        question_id = insertQuestion("What does the following sign when it flashes show you?", R.drawable.s1);
        insertQuestionAnswer(question_id, "The police have blockage ahead.", QTYPESIGNS, allVehicles, onlySetA);
        insertQuestionAnswer(question_id, "There is danger in the road ahead.", QTYPESIGNS, allVehicles, onlySetA, true);
        insertQuestionAnswer(question_id, "There is a robot ahead.", QTYPESIGNS, allVehicles, onlySetA);

        /**
         * MOTORCYCLE
         * Set A - 4 RULES
         * 4 QUESTIONS
         */
        question_id = insertQuestion("What is legal with regard to the towing of another vehicle?");
        insertQuestionAnswer(question_id, "You may tow it with a 750 cc motor cycle.", QTYPERULES, onlyMotorcycle, onlySetA, true);
        insertQuestionAnswer(question_id, "You may tow it with a tricycle.", QTYPERULES, onlyMotorcycle, onlySetA);
        insertQuestionAnswer(question_id, "You may not tow it with any motor cycle.", QTYPERULES, onlyMotorcycle, onlySetA);

        question_id = insertQuestion("You violate the traffic law if you ride your motor cycle or motor tricycle…");
        insertQuestionAnswer(question_id, "Conveying 2 adult people on the sidecar.", QTYPERULES, onlyMotorcycle, onlySetA, true);
        insertQuestionAnswer(question_id, "With a code 1 learner’s license without supervision on a motor cycle with a sidecar, while seating is available in the sidecar.", QTYPERULES, onlyMotorcycle, onlySetA);
        insertQuestionAnswer(question_id, "Next to another motor cycle in a one-way street while both of you are traveling in a separate lane.", QTYPERULES, onlyMotorcycle, onlySetA);

        question_id = insertQuestion("Your motor cycle must have…");
        insertQuestionAnswer(question_id, "1 number plate in the front only", QTYPERULES, onlyMotorcycle, onlySetA, true);
        insertQuestionAnswer(question_id, "1 number plate in the front and 1 at the rear.", QTYPERULES, onlyMotorcycle, onlySetA);
        insertQuestionAnswer(question_id, "1 number plate at the rear only.", QTYPERULES, onlyMotorcycle, onlySetA);

        question_id = insertQuestion("When is it an offence when you are the holder of a learner’s license for a motor cycle?\n" +
            "\n" +
            "(i) You ride on your own motor cycle next to your instructor.\n" +
            "(ii) You keep only one hand on the handle-bars.\n" +
            "(iii) You ride at 80 km/h outside build-up areas.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, onlyMotorcycle, onlySetA);
        insertQuestionAnswer(question_id, "(ii) Only is correct", QTYPERULES, onlyMotorcycle, onlySetA);
        insertQuestionAnswer(question_id, "(i) and (ii) only are correct", QTYPERULES, onlyMotorcycle, onlySetA);

        /**
         * MOTORCYCLE
         * Set A - 4 SIGNS
         * Set B - 4 SIGNS
         */
        //Sign L1 prohibits…
        question_id = insertQuestion("The following sign prohibits…", R.drawable.l1);
        insertQuestionAnswer(question_id, "All motor cycle riders to ride there.", QTYPESIGNS, onlyMotorcycle, onlySetA);
        insertQuestionAnswer(question_id, "Motor cycle with an engine capacity of 125 cc and less to be there.", QTYPESIGNS, onlyMotorcycle, onlySetA, true);
        insertQuestionAnswer(question_id, "Motor cycle with an engine capacity of more than 125 cc to be there.", QTYPESIGNS, onlyMotorcycle, onlySetA);

        //Sign L3 shows you that…
        question_id = insertQuestion("The following sign shows you that…", R.drawable.l3);
        insertQuestionAnswer(question_id, "They must ride to the left of this sign.", QTYPESIGNS, onlyMotorcycle, setABC, true);
        insertQuestionAnswer(question_id, "They can ride to the left of this sign if they wish.", QTYPESIGNS, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "The freeway ends ahead to the left.", QTYPESIGNS, onlyMotorcycle, setABC);

        //Sign G8 shows to motor cycle riders that…
        question_id = insertQuestion("The following sign shows motor cycle riders that…", R.drawable.g8);
        insertQuestionAnswer(question_id, "They must use that part of the road.", QTYPESIGNS, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "They can use that part of the road if they wish.", QTYPESIGNS, onlyMotorcycle, setABC, true);
        insertQuestionAnswer(question_id, "There is parking for motor cycle there.", QTYPESIGNS, onlyMotorcycle, setABC);

        //Sign J10 shows you that…
        question_id = insertQuestion("The following sign shows you that…", R.drawable.j10);
        insertQuestionAnswer(question_id, "You are riding in a city area.", QTYPESIGNS, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "It is recommended that you do not ride faster than 60 km/h.", QTYPESIGNS, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "The speed limit for you is 60 km/h.", QTYPESIGNS, onlyMotorcycle, setABC, true);

        //If you see C2, it means that…
        question_id = insertQuestion("If you see The following sign, it means that…", R.drawable.c2);
        insertQuestionAnswer(question_id, "You must ride there.", QTYPESIGNS, onlyMotorcycle, onlySetB, true);
        insertQuestionAnswer(question_id, "You can ride there but it is not compulsory to do so.", QTYPESIGNS, onlyMotorcycle, onlySetB);
        insertQuestionAnswer(question_id, "Only certain motor cycles may ride there.", QTYPESIGNS, onlyMotorcycle, onlySetB);

        /**
         * MOTORCYCLE
         * Set B - 4 RULES OF THE ROAD
         */
        question_id = insertQuestion("If your motor cycle has an engine size of… cc, you may no use it on a free way.");
        insertQuestionAnswer(question_id, "50", QTYPERULES, onlyMotorcycle, setBC, true);
        insertQuestionAnswer(question_id, "125", QTYPERULES, onlyMotorcycle, setBC);
        insertQuestionAnswer(question_id, "250", QTYPERULES, onlyMotorcycle, setBC);

        question_id = insertQuestion("When is it an offence when you are the holder of a learner’s license for a motor cycle?\n" +
            "\n" +
            "(i) You ride on your own motor cycle next to your instructor.\n" +
            "(ii) You keep only one hand on the handle-bars.\n" +
            "(iii) You ride at 80 km/h outside build-up areas.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "Only (i) and (ii) are correct", QTYPERULES, onlyMotorcycle, setBC);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, onlyMotorcycle, setBC);
        insertQuestionAnswer(question_id, "None of the above is correct", QTYPERULES, onlyMotorcycle, setBC, true);

        question_id = insertQuestion("When may you as the holder of a learner’s license carry a passenger on your motor cycle?");
        insertQuestionAnswer(question_id, "For short journeys if you do not exceed the speed limit.", QTYPERULES, onlyMotorcycle, setBC, true);
        insertQuestionAnswer(question_id, "In no case.", QTYPERULES, onlyMotorcycle, setBC, true);
        insertQuestionAnswer(question_id, "When a motor cycle has a cylinder capacity of at least 100 cc.", QTYPERULES, onlyMotorcycle, setBC);

        question_id = insertQuestion("When you ride your motor cycle, you are not allowed to…");
        insertQuestionAnswer(question_id, "Have only one hand on the handlebars.", QTYPERULES, onlyMotorcycle, setBC);
        insertQuestionAnswer(question_id, "Have one wheel of the motor cycle off the ground.", QTYPERULES, onlyMotorcycle, setBC, true);
        insertQuestionAnswer(question_id, "Carry a passenger on a 125 cc motor cycle.", QTYPERULES, onlyMotorcycle, setBC);

        /**
         * MOTORCYCLE
         * Set A - 7 CONTROL
         * Set B - 7 CONTROL
         */
        question_id = insertQuestion("To ride faster, you must use number…", R.drawable.motorcycle_controls);
        insertQuestionAnswer(question_id, "7", QTYPECONTROL, onlyMotorcycle, setABC, true);
        insertQuestionAnswer(question_id, "5", QTYPECONTROL, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "4", QTYPECONTROL, onlyMotorcycle, setABC);

        question_id = insertQuestion("To turn, you must use number…", R.drawable.motorcycle_controls);
        insertQuestionAnswer(question_id, "8", QTYPECONTROL, onlyMotorcycle, setABC, true);
        insertQuestionAnswer(question_id, "1", QTYPECONTROL, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "7", QTYPECONTROL, onlyMotorcycle, setABC);

        question_id = insertQuestion("To stop, you must use number…", R.drawable.motorcycle_controls);
        insertQuestionAnswer(question_id, "4 and 7", QTYPECONTROL, onlyMotorcycle, setABC, true);
        insertQuestionAnswer(question_id, "2 and 7", QTYPECONTROL, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "1 and 2", QTYPECONTROL, onlyMotorcycle, setABC);

        question_id = insertQuestion("The change gears, you must use numbers…", R.drawable.motorcycle_controls);
        insertQuestionAnswer(question_id, "1 and 5.", QTYPECONTROL, onlyMotorcycle, setABC, true);
        insertQuestionAnswer(question_id, "2 and 7.", QTYPECONTROL, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "1 and 2.", QTYPECONTROL, onlyMotorcycle, setABC);

        question_id = insertQuestion("To indicate that you are going to turn, you must use number…", R.drawable.motorcycle_controls);
        insertQuestionAnswer(question_id, "6.", QTYPECONTROL, onlyMotorcycle, setABC, true);
        insertQuestionAnswer(question_id, "4.", QTYPECONTROL, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "8.", QTYPECONTROL, onlyMotorcycle, setABC);

        question_id = insertQuestion("What controls must you use when you are going to make a sharp turn?", R.drawable.motorcycle_controls);
        insertQuestionAnswer(question_id, "1, 3, 5, 6 and 8 only", QTYPECONTROL, onlyMotorcycle, setABC, true);
        insertQuestionAnswer(question_id, "1, 2, 4 and 8 only", QTYPECONTROL, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "1, 2, 3, 4, 6 and 8 only", QTYPECONTROL, onlyMotorcycle, setABC);

        question_id = insertQuestion("What control must you never use in combination?", R.drawable.motorcycle_controls);
        insertQuestionAnswer(question_id, "2 and 4", QTYPECONTROL, onlyMotorcycle, setABC, true);
        insertQuestionAnswer(question_id, "4 and 8", QTYPECONTROL, onlyMotorcycle, setABC);
        insertQuestionAnswer(question_id, "4 and 7", QTYPECONTROL, onlyMotorcycle, setABC);

        /**
         * LIGHT MOTOR VEHICLE
         * Set A - 4 RULES
         * Set B - 4 RULES
         */
        question_id = insertQuestion("Which is ALLOWED when towing another vehicle?\n" +
            "\n" +
            "(i) A motor car tows another motorcar with a rope and drive 40 km/h.\n" +
            "(ii) You tow another vehicle with tow-bar.\n" +
            "(iii) A tractor tows a trailer with 10 passengers on it and drive 30 km/h\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, onlyLightMotorvehicle, onlySetA);
        insertQuestionAnswer(question_id, "(ii) and (iii) only are correct", QTYPERULES, onlyLightMotorvehicle, onlySetA, true);
        insertQuestionAnswer(question_id, "(i) Only is correct", QTYPERULES, onlyLightMotorvehicle, onlySetA);

        question_id = insertQuestion("What is true with regard to seat belts?\n" +
            "\n" +
            "(i) If your vehicle has seat belts in the rear, it must be worn.\n" +
            "(ii) You heed not wear a seat belt when reversing.\n" +
            "(iii) Children younger than 14 years need not wear seat belts.\n" +
            "(iv) If the front seat has a seat belt, your only passenger may not sit at the back where there is no seat belt.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, onlyLightMotorvehicle, onlySetA);
        insertQuestionAnswer(question_id, "(i), (ii) and (iv) only are correct", QTYPERULES, onlyLightMotorvehicle, onlySetA, true);
        insertQuestionAnswer(question_id, "(i) Only is correct", QTYPERULES, onlyLightMotorvehicle, onlySetA);

        question_id = insertQuestion("If you only have a learner’s license for light motor vehicle…\n" +
            "\n" +
            "(i) There must be someone with you in the vehicle with the same driving license.\n" +
            "(ii) You are not allowed to drive on freeway.\n" +
            "(iii) No passengers are allowed with you in the vehicle.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, onlyLightMotorvehicle, onlySetA);
        insertQuestionAnswer(question_id, "Only (i) and (iii) are correct", QTYPERULES, onlyLightMotorvehicle, onlySetA);
        insertQuestionAnswer(question_id, "Only (i) is correct", QTYPERULES, onlyLightMotorvehicle, onlySetA, true);

        question_id = insertQuestion("The tread pattern of your vehicle’s tyres may not be less than… mm.");
        insertQuestionAnswer(question_id, "0.75", QTYPERULES, onlyLightMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "1", QTYPERULES, onlyLightMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "1.5", QTYPERULES, onlyLightMotorvehicle, setABC);

        question_id = insertQuestion("The maximum distance between two vehicles, which are being towed, is… m");
        insertQuestionAnswer(question_id, "1.8", QTYPERULES, onlyLightMotorvehicle, setBC);
        insertQuestionAnswer(question_id, "2.5", QTYPERULES, onlyLightMotorvehicle, setBC);
        insertQuestionAnswer(question_id, "3.5", QTYPERULES, onlyLightMotorvehicle, setBC, true);

        question_id = insertQuestion("The lights of your vehicle parked on a public road between sunset and sunrise need not kept lighted when the vehicle is parked…\n" +
            "\n" +
            "(i) 10m from a lighted street lamp.\n" +
            "(ii) Next to the roadway of the road.\n" +
            "(iii) In a demarcated parking area.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, onlyLightMotorvehicle, setBC, true);
        insertQuestionAnswer(question_id, "Only (i) and (iii) are correct", QTYPERULES, onlyLightMotorvehicle, setBC);
        insertQuestionAnswer(question_id, "Only (ii) and (iii) are correct", QTYPERULES, onlyLightMotorvehicle, setBC);

        question_id = insertQuestion("It is illegal when you drive and a passenger…");
        insertQuestionAnswer(question_id, "Sits directly behind you when you only have a learner’s license.", QTYPERULES, onlyLightMotorvehicle, setBC);
        insertQuestionAnswer(question_id, "Fiddles with the motor radio’s volume knob.", QTYPERULES, onlyLightMotorvehicle, setBC);
        insertQuestionAnswer(question_id, "Rides on the bumper of your vehicle.", QTYPERULES, onlyLightMotorvehicle, setBC, true);

        /**
         * LIGHT MOTOR VEHICLE
         * Set A - 4 SIGNS
         * Set B - 4 SIGNS
         */
        //Sign K3 shows you that you…
        question_id = insertQuestion("The following sign shows you that you…", R.drawable.k3);
        insertQuestionAnswer(question_id, "May not drive there between 06:00 and 09:00.", QTYPESIGNS, onlyLightMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "May not park there for more than 3 hour.", QTYPESIGNS, onlyLightMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "Are only allowed to drive there between 06:00 and 09:00.", QTYPESIGNS, onlyLightMotorvehicle, setABC);

        //Sign C3 shows you that…
        question_id = insertQuestion("The following sign shows you that…", R.drawable.c3);
        insertQuestionAnswer(question_id, "The area is for motorcar taxis only.", QTYPESIGNS, onlyLightMotorvehicle, setAC);
        insertQuestionAnswer(question_id, "You can drive there if you wish.", QTYPESIGNS, onlyLightMotorvehicle, setAC);
        insertQuestionAnswer(question_id, "You must drive there.", QTYPESIGNS, onlyLightMotorvehicle, setAC, true);

        //Sign F5 shows you that…
        question_id = insertQuestion("The following sign shows you that…", R.drawable.f5);
        insertQuestionAnswer(question_id, "No taxis may drive there.", QTYPESIGNS, onlyLightMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "No motorcars may drive there.", QTYPESIGNS, onlyLightMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "You may not park your motorcar there.", QTYPESIGNS, onlyLightMotorvehicle, setABC);

        //Sign G9 shows you that…
        question_id = insertQuestion("The following sign shows you that…", R.drawable.g9);
        insertQuestionAnswer(question_id, "You must use that part of the road.", QTYPESIGNS, onlyLightMotorvehicle, setAB);
        insertQuestionAnswer(question_id, "You can use that part of the road if you wish.", QTYPESIGNS, onlyLightMotorvehicle, setAB, true);
        insertQuestionAnswer(question_id, "There is parking for motorcars there.", QTYPESIGNS, onlyLightMotorvehicle, setAB);

        //Sign F6 forbids…
        question_id = insertQuestion("The following sign forbids…", R.drawable.f6);
        insertQuestionAnswer(question_id, "Minibuses to drive past this sign.", QTYPESIGNS, onlyLightMotorvehicle, setBC, true);
        insertQuestionAnswer(question_id, "All motor vehicles to drive past this sign.", QTYPESIGNS, onlyLightMotorvehicle, setBC);
        insertQuestionAnswer(question_id, "Minibuses to pick up passengers.", QTYPESIGNS, onlyLightMotorvehicle, setBC);

        /**
         * LIGHT MOTOR VEHICLE
         * HEAVY MOTOR VEHICLE
         * Set A - 5 CONTROL
         * Set B - 5 CONTROL
         */
        question_id = insertQuestion("To select a gear, you must use numbers…");
        insertQuestionAnswer(question_id, "7 & 9.", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "5 & 8.", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "6 & 8.", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC, true);

        question_id = insertQuestion("To stop your vehicle, you must use number…");
        insertQuestionAnswer(question_id, "9", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "8", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "7", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);

        question_id = insertQuestion("To ensure that your parked vehicle does not move, use number…");
        insertQuestionAnswer(question_id, "7", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "8", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "9", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);

        question_id = insertQuestion("To accelerate your vehicle, you must use number…");
        insertQuestionAnswer(question_id, "6", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "8", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "10", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC, true);

        question_id = insertQuestion("What controls must you use when you are going to turn sharp?");
        insertQuestionAnswer(question_id, "1, 3, 5, 6 and 8 only", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "3, 4, 5, 9 and 10 only", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "1, 3, 4, 5, 6, 8, 9 and 10 only", QTYPECONTROL, lightAndHeavyMotorvehicle, setABC, true);

        /**
         * LIGHT MOTOR VEHICLE
         * Set A - 2 CONTROL
         * Set B - 2 CONTROL
         */
        question_id = insertQuestion("Number… is not found in an automatic vehicle.", R.drawable.light_motor_vehicle_controls);
        insertQuestionAnswer(question_id, "2", QTYPECONTROL, onlyLightMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "6", QTYPECONTROL, onlyLightMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "8", QTYPECONTROL, onlyLightMotorvehicle, setABC, true);

        question_id = insertQuestion("To turn, number… is used.");
        insertQuestionAnswer(question_id, "4", QTYPECONTROL, onlyLightMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "5", QTYPECONTROL, onlyLightMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "10", QTYPECONTROL, onlyLightMotorvehicle, setABC);

        /**
         * ALL VEHICLES
         * Set B - RULES & SIGNS
         * 37 QUESTIONS
         *
         * 3 signs added to existing signs
         * 12 Rules added to existing questions
         */
        question_id = insertQuestion("When are you allowed to drive your vehicle on the right-hand side of a road with traffic in both directions?");
        insertQuestionAnswer(question_id, "When you switch on the emergency lights of your vehicle on.", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "When a traffic officer orders you to do so.", QTYPERULES, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "Under no circumstances.", QTYPERULES, allVehicles, setBC);

        question_id = insertQuestion("At an intersection…");
        insertQuestionAnswer(question_id, "You must yield to oncoming traffic if you want to turn right.", QTYPERULES, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "Vehicles have right of way over pedestrians.", QTYPERULES, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "You can use a stop sign as a yield sign if there is no other traffic.", QTYPERULES, allVehicles, onlySetB);

        question_id = insertQuestion("If you see that someone wants to overtake you, you must…\n" +
            "\n" +
            "(i) Keep to the left as far as is safe.\n" +
            "(ii) Not drive faster.\n" +
            "(iii) Give hand signals to allow the person to pass safely.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "(i) and (ii) only are correct", QTYPERULES, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "(ii) only is correct", QTYPERULES, allVehicles, onlySetB);

        question_id = insertQuestion("When you drive…");
        insertQuestionAnswer(question_id, "You must have two hands on the steering wheel.", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "Your vision of the road and traffic must be unobstructed.", QTYPERULES, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "You must wear shoes with rubber soles.", QTYPERULES, allVehicles, setBC);

        question_id = insertQuestion("Where may you legally stop with your vehicle?");
        insertQuestionAnswer(question_id, "4 m from a tunnel", QTYPERULES, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "5 m from a pedestrian crossing", QTYPERULES, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "6 m from a railway crossing", QTYPERULES, allVehicles, onlySetB, true);

        question_id = insertQuestion("You may not…\n" +
            "\n" +
            "(i) Run the engine of your vehicle unattended.\n" +
            "(ii) Use your vehicle without a cap on the fuel tank.\n" +
            "(iii) Spin the wheels of your vehicle when pulling off.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "(i) and (ii) only are correct", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "(ii) only is correct", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, allVehicles, setBC, true);

        question_id = insertQuestion("What is the maximum period of time that a vehicle may be parked in one place on a road outside urban areas?");
        insertQuestionAnswer(question_id, "7 days", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "48 hours", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "24 hours", QTYPERULES, allVehicles, setBC, true);

        question_id = insertQuestion("If you come to the traffic and the red light flashes, you must…");
        insertQuestionAnswer(question_id, "Stop and wait for the light to change to green before you go.", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "Stop and go only of it is safe to do so.", QTYPERULES, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "Lookout for a roadblock as the light shows you a police stop.", QTYPERULES, allVehicles, setBC);

        question_id = insertQuestion("The road markings RTM3 in the sketch…", R.drawable.rtm3);
        insertQuestionAnswer(question_id, "Shows you that you may not stop further than the front one of the two.", QTYPERULES, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "Are the lines where pedestrians walk.", QTYPERULES, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Are lines that indicate another traffic lane.", QTYPERULES, allVehicles, onlySetB);

        question_id = insertQuestion("What is important with regard to the hooter of your vehicle?");
        insertQuestionAnswer(question_id, "The tone of pitch of the sound may not change.", QTYPERULES, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "Someone must hear it from a distance of at least 45 m.", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "You may use it to get attentio0n of someone that you would like to offer a lift.", QTYPERULES, allVehicles, setBC);

        question_id = insertQuestion("You may…");
        insertQuestionAnswer(question_id, "Drive your vehicle on the sidewalk.", QTYPERULES, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Allow your vehicle to move backwards only if it is safe to do so.", QTYPERULES, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "Leave the engine of your vehicle running when you put petrol in it.", QTYPERULES, allVehicles, onlySetB);

        question_id = insertQuestion("You may overtake another vehicle on the left-hand side…\n" +
            "\n" +
            "(i) When that vehicle is going to turn right and the road is wide enough that it is not necessary to\n" +
            "drive on the shoulder.\n" +
            "(ii) Where the road has two lanes for traffic in the same direction.\n" +
            "(iii) If a police officer instructs you to do so.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "(iii) only is correct", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "(ii) and (iii) only are correct", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, allVehicles, setBC, true);

        question_id = insertQuestion("The license for your vehicle (clearance certificate) is valid for…");
        insertQuestionAnswer(question_id, "12 months.", QTYPERULES, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "90 days.", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "21 days.", QTYPERULES, allVehicles, setBC);

        question_id = insertQuestion("If you are driving towards Z4 and there are also vehicles from the other three sides, you…", R.drawable.z4);
        insertQuestionAnswer(question_id, "Must always wait for the vehicle from you right before you drive on.", QTYPERULES, allVehicles, setBC);
        insertQuestionAnswer(question_id, "May drive on if you were the first vehicle over the line.", QTYPERULES, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "Must stop with all the vehicles and only drive when it is safe.", QTYPERULES, allVehicles, setBC);

        //Look in the sketch booklet, sketch 6. When you see road-marking (a), you…
        question_id = insertQuestion("When you see road-marking (a), you…", R.drawable.sketch6);
        insertQuestionAnswer(question_id, "May not park there because it is parking for ambulances only.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "May not park there at all.", QTYPESIGNS, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "Know it is a lane reserved for emergency vehicles.", QTYPESIGNS, allVehicles, setBC);

        //Arrow E in the sketch booklet, sketch 5, shows you that you…
        question_id = insertQuestion("Arrow E in the sketch, shows you that you…", R.drawable.sketch5);
        insertQuestionAnswer(question_id, "Must turn right from that lane.", QTYPESIGNS, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "Can turn right or move straight on if it is safe.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "Can still change lanes if you want to go on straight.", QTYPESIGNS, allVehicles, setBC);

        //Sign O9 warns you that there is…
        question_id = insertQuestion("The following sign warns you that there is…", R.drawable.o9);
        insertQuestionAnswer(question_id, "Possibly pedestrians ahead.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "A marked pedestrian crossing ahead.", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "Possibly school kids close to or in the road.", QTYPESIGNS, allVehicles, onlySetB);

        //Sign B8 shows you that you…
        question_id = insertQuestion("The following sign shows you that you…", R.drawable.b8);
        insertQuestionAnswer(question_id, "Can expect a sharp bent to the right.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Will get a one-way road to the right.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Must turn right at the next road.", QTYPESIGNS, allVehicles, onlySetB, true);

        //Sign Q7 shows you that…
        question_id = insertQuestion("The following sign shows you that…", R.drawable.q7);
        insertQuestionAnswer(question_id, "The road narrows from both sides.", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "The freeway ends.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "There is a narrow bridge ahead.", QTYPESIGNS, allVehicles, onlySetB);

        question_id = insertQuestion("Road marking RTM 1 in the sketch shows you that you, …", R.drawable.rtm1);
        insertQuestionAnswer(question_id, "A. If you are driving in car A, must stop before this line.", QTYPESIGNS, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "If you are driving in car A, must reduce speed and drive on if is safe.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "If you are driving in car B, can pass car A if there are no other vehicles in the intersection.", QTYPESIGNS, allVehicles, setBC);

        question_id = insertQuestion("The following warning sign shows you that…", R.drawable.s3);
        insertQuestionAnswer(question_id, "There is a possibility of a flood ahead.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "The road end because of water ahead.", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "There is a low water bridge ahead.", QTYPESIGNS, allVehicles, onlySetB);

        question_id = insertQuestion("The following road marking shows you that…", R.drawable.y3);
        insertQuestionAnswer(question_id, "The road surface is uneven.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "Traffic may not pass or cross it on either side.", QTYPESIGNS, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "It is a lane reserved for buses only.", QTYPESIGNS, allVehicles, setBC);

        question_id = insertQuestion("When you want to change lanes and drive from L1 to L2 you must…\n" +
            "\n" +
            "(i) Only do it when it is safe to do so.\n" +
            "(ii) Switch your indicators on in time to show what you are going to do.\n" +
            "(iii) Use the mirror(s) of your vehicle to ensure that you know of other traffic.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION", R.drawable.sketch8);
        insertQuestionAnswer(question_id, "Only (i) and (ii) are correct", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "All the above are correct", QTYPESIGNS, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "Only (ii) and (iii) are correct", QTYPESIGNS, allVehicles, setBC);

        question_id = insertQuestion("When you see the following sign you must…", R.drawable.a6);
        insertQuestionAnswer(question_id, "Know that, that section of the road is for the use of pedestrians only.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Give right of way to pedestrians who would like to cross the street.", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "Stop because school kids might cross the road.", QTYPESIGNS, allVehicles, onlySetB);

        //Sign R1 warns you that the road…
        question_id = insertQuestion("The following sign warns you that the road…", R.drawable.r1);
        insertQuestionAnswer(question_id, "Has many curves.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Is slippery when it is wet.", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "It’s surface is damaged.", QTYPESIGNS, allVehicles, onlySetB);

        //Sign O4 warns you that…
        question_id = insertQuestion("The following sign warns you that…", R.drawable.o4);
        insertQuestionAnswer(question_id, "There is a compulsory police stop ahead.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "There is a robot ahead.", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "You are now entering an urban area.", QTYPESIGNS, allVehicles, onlySetB);

        question_id = insertQuestion("What does the following warning sign shows you?", R.drawable.o3);
        insertQuestionAnswer(question_id, "The road winds ahead.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "A number of sharp curves is ahead.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "A concealed entrance to the left is followed by one to the right.", QTYPESIGNS, allVehicles, setBC, true);

        //Sign BB4 shows you that only… may park there.
        question_id = insertQuestion("The following sign shows you that only… may park there.", R.drawable.bb4);
        insertQuestionAnswer(question_id, "Goods vehicles that load or unload", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "Motorcars that load or unload", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Construction vehicle", QTYPESIGNS, allVehicles, onlySetB);

        question_id = insertQuestion("When you see the following sign you must…", R.drawable.s7);
        insertQuestionAnswer(question_id, "Look out for road works where tractors operate.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Know that only agricultural vehicles may drive there.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Be on the look out for agricultural vehicles that may be on the road.", QTYPESIGNS, allVehicles, onlySetB, true);

        question_id = insertQuestion("The following warning sign shows you that…", R.drawable.q4);
        insertQuestionAnswer(question_id, "The road on which you are driving is going to change to gravel road.", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "Potholes are to be found on the road ahead.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "The road is ending ahead.", QTYPESIGNS, allVehicles, onlySetB);

        question_id = insertQuestion("What does the following sign show you?", R.drawable.j8);
        insertQuestionAnswer(question_id, "You are not allowed to travel faster than 100 km/h at night.", QTYPESIGNS, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "If you cannot see more than 100 m in front of you. Switch your lights on.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "Dangerous conditions for the next 100 km.", QTYPESIGNS, allVehicles, setBC);

        question_id = insertQuestion("If you see the following sign know that…", R.drawable.b2);
        insertQuestionAnswer(question_id, "Vehicles with a mass of 10 tonnes and less may not drive there.", QTYPESIGNS, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "Vehicles with a mass of more than 10 tonnes and less may not drive there.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "That section of the road can only carry vehicles that weigh up to 10 tonnes.", QTYPESIGNS, allVehicles, setBC);

        //Sign N3 shows you that there is a…
        question_id = insertQuestion("The following sign shows you that there is a…", R.drawable.n3);
        insertQuestionAnswer(question_id, "Sharp curve to the left ahead.", QTYPESIGNS, allVehicles, setBC, true);
        insertQuestionAnswer(question_id, "Obstruction to the left of the road.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "Detour to the left.", QTYPESIGNS, allVehicles, setBC);

        question_id = insertQuestion("The following road marking shows you that you must…", R.drawable.y1);
        insertQuestionAnswer(question_id, "Stop.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Look out for school kids.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Yield to all user of the road and possibly also trains", QTYPESIGNS, allVehicles, onlySetB, true);

        //Sign DD7 shows you that…
        question_id = insertQuestion("The following sign shows you that…", R.drawable.dd7);
        insertQuestionAnswer(question_id, "The road turning to the right, ends ahead.", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "You are not allowed to turn right there.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "There is a T-junction to the right.", QTYPESIGNS, allVehicles, onlySetB);

        //Sign T2 shows you that there is a…
        question_id = insertQuestion("The following sign shows you that there is a…", R.drawable.t2);
        insertQuestionAnswer(question_id, "Curve to the left ahead.", QTYPESIGNS, allVehicles, onlySetB);
        insertQuestionAnswer(question_id, "Temporary obstruction to the left of the road.", QTYPESIGNS, allVehicles, onlySetB, true);
        insertQuestionAnswer(question_id, "Temporary detour to the left.", QTYPESIGNS, allVehicles, onlySetB);

        //Sign A2 shows you that you must…
        question_id = insertQuestion("The following sign shows you that you must…", R.drawable.a2);
        insertQuestionAnswer(question_id, "Turn left at the stop sign.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "Stop and then turn left or drive strait on.", QTYPESIGNS, allVehicles, setBC);
        insertQuestionAnswer(question_id, "Stop but if you want to turn left, you can use it as a yield sign.", QTYPESIGNS, allVehicles, setBC, true);

        /**
         * HEAVY MOTOR VEHICLE
         * Set A - 4 RULES
         * Set B - 4 RULES
         */
        question_id = insertQuestion("You may transport goods on your vehicle that they…");
        insertQuestionAnswer(question_id, "Protrude 2.0 m at the rear.", QTYPERULES, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "Protrude 450 mm in front.", QTYPERULES, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "Are with the vehicle 2, 4 m wide.", QTYPERULES, onlyHeavyMotorvehicle, setABC, true);

        question_id = insertQuestion("You may not reverse your vehicle for further than…");
        insertQuestionAnswer(question_id, "500 m.", QTYPERULES, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "100 m.", QTYPERULES, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "As it is safe to do so.", QTYPERULES, onlyHeavyMotorvehicle, setABC, true);

        question_id = insertQuestion("If you only have learner’s license for a heavy motor vehicle, …\n" +
            "\n" +
            "(i) There must be someone with you in the vehicle with the same driving license.\n" +
            "(ii) You are not allowed to drive on a freeway.\n" +
            "(iii) No passengers are allowed with you in the vehicle.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "All the above are correct", QTYPERULES, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "Only (i) is correct", QTYPERULES, onlyHeavyMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "Only (i) and (iii) are correct", QTYPERULES, onlyHeavyMotorvehicle, setABC);

        question_id = insertQuestion("You may carry passengers in a vehicle that is being towed…\n" +
            "\n" +
            "(i) if you only travel at 35 km/h.\n" +
            "(ii) if the vehicle that is being towed is a semi-trailer.\n" +
            "(iii) Only if the vehicle that tows something is a tractor.\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION");
        insertQuestionAnswer(question_id, "(i) and (ii) only are correct", QTYPERULES, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPERULES, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "(ii) only is correct", QTYPERULES, onlyHeavyMotorvehicle, setABC, true);

        /**
         * HEAVY MOTOR VEHICLE
         * Set A - 4 SIGNS
         * Set B - 4 SIGNS
         */
        //Sign B2 shows you that if your vehicle has a GVM of…
        question_id = insertQuestion("The following sign shows you that if your vehicle has a GVM of…", R.drawable.b2);
        insertQuestionAnswer(question_id, "8t, you must drive there at all times.", QTYPESIGNS, onlyHeavyMotorvehicle, onlySetA, true);
        insertQuestionAnswer(question_id, "12t, you must drive there at all times.", QTYPESIGNS, onlyHeavyMotorvehicle, onlySetA);
        insertQuestionAnswer(question_id, "10t, you must drive there whenever you want to.", QTYPESIGNS, onlyHeavyMotorvehicle, onlySetA);

        //Sign K2 shows that…
        question_id = insertQuestion("The following sign shows that…", R.drawable.k2);
        insertQuestionAnswer(question_id, "All heavy vehicles may drive here during the times showed on the sign.", QTYPESIGNS, onlyHeavyMotorvehicle, setAC);
        insertQuestionAnswer(question_id, "Tankers must use road during the times showed on the sign.", QTYPESIGNS, onlyHeavyMotorvehicle, setAC);
        insertQuestionAnswer(question_id, "Vehicles carrying hazardous products may only use the road during the times showed on the sign.", QTYPESIGNS, onlyHeavyMotorvehicle, setAC, true);

        //Sign D10 shows you that.
        question_id = insertQuestion("The following sign shows you that…", R.drawable.d10);
        insertQuestionAnswer(question_id, "Vehicles longer than 15 m may not drive past this sign.", QTYPESIGNS, onlyHeavyMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "The road is only 15 m wide past this sign.", QTYPESIGNS, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "No vehicles with trailers may drive past this sign.", QTYPESIGNS, onlyHeavyMotorvehicle, setABC);

        //Sign W6 shows you that…
        question_id = insertQuestion("The following sign shows you that…", R.drawable.w6);
        insertQuestionAnswer(question_id, "The road forks ahead on a steep decline and you should slow down.", QTYPESIGNS, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "Your brakes will be checked at a stop ahead where you must stop.", QTYPESIGNS, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "There is an arrestor bed ahead to be used when your brakes fail.", QTYPESIGNS, onlyHeavyMotorvehicle, setABC, true);

        //Sign P9 warns you that you may not drive there if your vehicle is…
        question_id = insertQuestion("The following sign warns you that you may not drive there if your vehicle is…", R.drawable.p9);
        insertQuestionAnswer(question_id, "Longer than 4.42 m.", QTYPESIGNS, onlyHeavyMotorvehicle, setBC);
        insertQuestionAnswer(question_id, "Higher than 4.42 m.", QTYPESIGNS, onlyHeavyMotorvehicle, setBC, true);
        insertQuestionAnswer(question_id, "Wider than 4.42 m.", QTYPESIGNS, onlyHeavyMotorvehicle, setBC);

        //Sign G2 prohibits all… driving there.
        question_id = insertQuestion("The following sign prohibits all… driving there.\n" +
            "\n" +
            "(i) Vehicles conveying dangerous goods\n" +
            "(ii) Goods vehicle\n" +
            "(iii) Abnormal vehicles\n" +
            "(iv) Heavy vehicles\n" +
            "\n" +
            "SELECT THE CORRECT COMBINATION", R.drawable.g2);
        insertQuestionAnswer(question_id, "All of the above are correct", QTYPESIGNS, onlyHeavyMotorvehicle, onlySetB);
        insertQuestionAnswer(question_id, "Only (i) is correct", QTYPESIGNS, onlyHeavyMotorvehicle, onlySetB, true);
        insertQuestionAnswer(question_id, "Only (iii) and (iv) are correct", QTYPESIGNS, onlyHeavyMotorvehicle, onlySetB);

        /**
         * HEAVY MOTOR VEHICLE
         * Set A - 2 CONTROL
         * Set B - 2 CONTROL
         */
        question_id = insertQuestion("To turn your vehicle, number… is used.");
        insertQuestionAnswer(question_id, "5", QTYPECONTROL, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "4", QTYPECONTROL, onlyHeavyMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "6", QTYPECONTROL, onlyHeavyMotorvehicle, setABC);

        question_id = insertQuestion("To indicate that you are going to turn, you must use number…");
        insertQuestionAnswer(question_id, "3", QTYPECONTROL, onlyHeavyMotorvehicle, setABC);
        insertQuestionAnswer(question_id, "5", QTYPECONTROL, onlyHeavyMotorvehicle, setABC, true);
        insertQuestionAnswer(question_id, "11", QTYPECONTROL, onlyHeavyMotorvehicle, setABC);

        /**
         * ALL VEHICLES
         * Set C - RULES & SIGNS
         * 19 QUESTIONS
         *
         * ? signs added to existing signs
         * ? Rules added to existing questions
         *
         * Rules of the Road questions: [26].Signs questions: [26]
         */
        //What does warning sign M1 show you?
        question_id = insertQuestion("What does the following warning sign show you?", R.drawable.m1);
        insertQuestionAnswer(question_id, "Roads cross ahead and you may have to stop or yield at the intersection.", QTYPESIGNS, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "A railway line intersects with road that you are traveling on.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "A 4-way-stop will be found ahead where the roads cross.", QTYPESIGNS, allVehicles, onlySetC);

        //Sign N8 shows you that there…
        question_id = insertQuestion("The following sign shows you that there…", R.drawable.n8);
        insertQuestionAnswer(question_id, "Is two-way traffic at the following road that crosses the road you are on.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Are two lanes ahead in different direction.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Is two-way traffic ahead on the one-way road you are traveling on.", QTYPESIGNS, allVehicles, onlySetC, true);

        question_id = insertQuestion("The only instance where you may stop on a freeway is…");
        insertQuestionAnswer(question_id, "To obey a road traffic sign.", QTYPERULES, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "For rest during a tiring journey.", QTYPERULES, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "To pick up hitchhikers.", QTYPERULES, allVehicles, onlySetC);

        //Sign B7 shows you that you…
        question_id = insertQuestion("The following sign shows you that you…", R.drawable.b7);
        insertQuestionAnswer(question_id, "Can expect a sharp bent to the left.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Must turn left at the next road.", QTYPESIGNS, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "Will get a one-way road to the left.", QTYPESIGNS, allVehicles, onlySetC);

        //Sign Q10 warns you that…
        question_id = insertQuestion("The following sign warns you that…", R.drawable.q10);
        insertQuestionAnswer(question_id, "The road is uneven.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "There are speed humps in the road.", QTYPESIGNS, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "There are potholes in the road.", QTYPESIGNS, allVehicles, onlySetC);

        question_id = insertQuestion("You are not allowed to stop…");
        insertQuestionAnswer(question_id, "On the pavement.", QTYPERULES, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "With the front of your vehicle facing oncoming traffic.", QTYPERULES, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "Next to an obstruction on the road.", QTYPERULES, allVehicles, onlySetC);

        question_id = insertQuestion("When you see the following sign, you must slow down because…", R.drawable.p6);
        insertQuestionAnswer(question_id, "The surface of the road is uneven on the left-hand side.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "There is dual track railway line in front.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "There is a motor gate on the left-hand side of the road.", QTYPESIGNS, allVehicles, onlySetC, true);

        question_id = insertQuestion("What does the following sign show you?", R.drawable.q6);
        insertQuestionAnswer(question_id, "The road temporarily narrows from both sides.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "The freeway temporarily ends ahead.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Only one vehicle can pass through the obstacle ahead.", QTYPESIGNS, allVehicles, onlySetC, true);

        question_id = insertQuestion("What does the following warning sign shows you?", R.drawable.u3);
        insertQuestionAnswer(question_id, "The road on which you are driving on is going to change to gravel road.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Potholes are to be found on the road ahead.", QTYPESIGNS, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "The road is ending ahead.", QTYPESIGNS, allVehicles, onlySetC);

        question_id = insertQuestion("The following marking shows you that… may park there.", R.drawable.bb8);
        insertQuestionAnswer(question_id, "Minibuses", QTYPESIGNS, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "Municipal buses", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Motor cycles", QTYPESIGNS, allVehicles, onlySetC);

        question_id = insertQuestion("The following warning sign shows you that…", R.drawable.s5);
        insertQuestionAnswer(question_id, "There is a possibility of a flood ahead.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "The road end because of water ahead.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "There is a low water bridge ahead.", QTYPESIGNS, allVehicles, onlySetC, true);

        question_id = insertQuestion("When you get to the following sign, you must…", R.drawable.t8);
        insertQuestionAnswer(question_id, "Choose to turn left or right or drive on straight after you have stopped.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Be ready to stop and turn left or right.", QTYPESIGNS, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "When you want to drive on straight, ignore it because traffic from the sides must stop.", QTYPESIGNS, allVehicles, onlySetC);

        question_id = insertQuestion("The following road marking is a…", R.drawable.y4);
        insertQuestionAnswer(question_id, "Pedestrian crossing where you must stop for pedestrians.", QTYPERULES, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Painted island where you are not allowed to drive on or stop.", QTYPERULES, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "Chevron that indicates a curve to the left.", QTYPERULES, allVehicles, onlySetC);

        question_id = insertQuestion("see sketch, vehicle B…", R.drawable.sketch4);
        insertQuestionAnswer(question_id, "Can drive past vehicle A if there are no other vehicles.", QTYPERULES, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Can drive over the stop line following vehicle A if there are no oncoming vehicles.", QTYPERULES, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Must stop behind vehicle A, drive nearer if that vehicle has driven off, stop immediately behind the stop line and drive on when it safe to do so.", QTYPERULES, allVehicles, onlySetC, true);

        question_id = insertQuestion("You may…");
        insertQuestionAnswer(question_id, "Leave your vehicle’s engine running without supervision.", QTYPERULES, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Allow someone to ride on time bumper of your vehicle.", QTYPERULES, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Put your arm out of the window only to give legal hand signals.", QTYPERULES, allVehicles, onlySetC, true);

        //Sign B1 shows you the...
        question_id = insertQuestion("The following sign shows you the...", R.drawable.b1);
        insertQuestionAnswer(question_id, "Fastest speed that you may drive.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Distance to the next off-ramp.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Slowest speed that you may drive.", QTYPESIGNS, allVehicles, onlySetC, true);

        question_id = insertQuestion("At an intersection...");
        insertQuestionAnswer(question_id, "You must yield to oncoming traffic if you want to turn right.", QTYPERULES, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "Vehicles have right of way over pedestrians.", QTYPERULES, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "You can use a stop sign as a yield sign if there is no other traffic.", QTYPERULES, allVehicles, onlySetC);

        //Sign T1 shows you that there is a/an…
        question_id = insertQuestion("The following sign shows you that there is a/an…", R.drawable.t1);
        insertQuestionAnswer(question_id, "Curve to the right ahead.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "Obstruction to the right of the road.", QTYPESIGNS, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "Detour to the right.", QTYPESIGNS, allVehicles, onlySetC);

        //Sign DD5 shows you that…
        question_id = insertQuestion("The following sign shows you that…", R.drawable.dd5);
        insertQuestionAnswer(question_id, "The road turning to the left, ends ahead.", QTYPESIGNS, allVehicles, onlySetC, true);
        insertQuestionAnswer(question_id, "You are not allowed to turn left there.", QTYPESIGNS, allVehicles, onlySetC);
        insertQuestionAnswer(question_id, "There is a T-junction to the left.", QTYPESIGNS, allVehicles, onlySetC);

        /**
         * MOTORCYCLE
         * Set C - 1 SIGNS
         */
        question_id = insertQuestion("When you see the following sign, you…", R.drawable.f4);
        insertQuestionAnswer(question_id, "May only ride a motor cycle of above 50 cc there.", QTYPESIGNS, onlyMotorcycle, onlySetC, true);
        insertQuestionAnswer(question_id, "Must yield for other motor vehicles.", QTYPESIGNS, onlyMotorcycle, onlySetC);
        insertQuestionAnswer(question_id, "May not ride there.", QTYPESIGNS, onlyMotorcycle, onlySetC);
    }

    /**
     * Insert the given question into the question table
     *
     * @param question
     * @return
     */
    public long insertQuestion(String question) {
        ContentValues values = new ContentValues();
        values.put(
            QUESTION_COLUMN_NAME,
            question
        );

        return database.insert(
            QUESTION_TABLE_NAME,
            null,
            values
        );
    }

    /**
     * Insert the given question and image into the question table
     *
     * @param question
     * @return
     */
    public long insertQuestion(String question, int image) {
        ContentValues values = new ContentValues();
        values.put(
            QUESTION_COLUMN_NAME,
            question
        );
        values.put(
            QUESTION_COLUMN_IMAGE,
            String.valueOf(image)
        );

        return database.insert(
            QUESTION_TABLE_NAME,
            null,
            values
        );
    }

    /**
     * This function will just call insert answer with the correct answer
     * flag set to false.
     *
     * @param question_id
     * @param answer
     * @param qtype_id
     * @param type_id
     * @param version_id
     *
     * @return void
     */
    public void insertQuestionAnswer(
        long question_id,
        String answer,
        int qtype_id,
        int[] type_id,
        int[] version_id
    ) {
        insertQuestionAnswer(question_id, answer, qtype_id, type_id, version_id, false);
    }

    public void insertQuestionAnswer(
        long question_id,
        String answer,
        int qtype_id,
        int[] type_ids,
        int[] version_ids,
        boolean correctAnswer
    ) {
        ContentValues values = new ContentValues();
        values.put(
            ANSWER_COLUMN_NAME,
            answer
        );

        long answer_id;

        // Check if the answer already exist in the db
        String sSql = "SELECT _id, name FROM answer WHERE name = \"" + answer + "\"";

        Cursor checkAnswer = database.rawQuery(sSql, null);

        if (checkAnswer.getCount() == 0) {
            // Answer does not exist, insert it
            answer_id = database.insert(
                ANSWER_TABLE_NAME,
                null,
                values
            );
        } else {
            checkAnswer.moveToFirst();

            // Answer exist already, use it
            answer_id = checkAnswer.getLong(
                checkAnswer.getColumnIndex(
                    "_id"
                )
            );
        }
        for(int version_id : version_ids) {

            for (int type_id : type_ids) {
                values = new ContentValues();
                values.put(
                    Q_A_COLUMN_ANSWER_ID,
                    answer_id
                );
                values.put(
                    Q_A_COLUMN_QUESTION_ID,
                    question_id
                );
                values.put(
                    Q_A_COLUMN_QTYPE_ID,
                    qtype_id
                );
                values.put(
                    Q_A_COLUMN_TYPE_ID,
                    type_id
                );
                values.put(
                    Q_A_COLUMN_VERSION_ID,
                    version_id
                );
                values.put(
                    Q_A_COLUMN_CORRECT_ANSWER,
                    correctAnswer
                );
                database.insert(
                    Q_A_TABLE_NAME,
                    null,
                    values
                );
            }
        }
    }

    /**
     * Get the question and answer details for a given question_id
     * and version id.
     *
     * @param version_id the version this is for, e.g. test set a, etc
     * @param type_id the type this question and answers are for, e.g. motorcyle, etc
     * @param bGroupByQuestionId whether we want to group by question id
     *
     * @return
     */
    public Cursor getQuestionAnswers(
        String version_id,
        String type_id,
        boolean bGroupByQuestionId
    ) {
        database = this.getReadableDatabase();

        String query = " SELECT "
            + " " + QUESTION_TABLE_NAME + "." + QUESTION_COLUMN_ID + ", "
            + " " + QUESTION_TABLE_NAME + "." + QUESTION_COLUMN_NAME + " AS "
            + QUESTION_COLUMN_NAME_ALIAS + ", "
            + " " + QUESTION_TABLE_NAME + "." + QUESTION_COLUMN_IMAGE + " AS "
            + QUESTION_COLUMN_IMAGE_ALIAS + ", "
            + " " + ANSWER_TABLE_NAME + "." + ANSWER_COLUMN_NAME + " AS "
            + ANSWER_COLUMN_NAME_ALIAS + ", "
            + " " + Q_A_TABLE_NAME + "." + Q_A_COLUMN_CORRECT_ANSWER + ", "
            + " " + Q_A_TABLE_NAME + "." + Q_A_COLUMN_ANSWERED_CORRECT + ", "
            + " " + Q_A_TABLE_NAME + "." + Q_A_COLUMN_QTYPE_ID
            + " FROM " + Q_A_TABLE_NAME
            + " INNER JOIN " + QUESTION_TABLE_NAME
            + "  ON " + Q_A_TABLE_NAME + "." + Q_A_COLUMN_QUESTION_ID + " = "
            + QUESTION_TABLE_NAME + "." + QUESTION_COLUMN_ID
            + " INNER JOIN " + ANSWER_TABLE_NAME
            + "  ON " + Q_A_TABLE_NAME + "." + Q_A_COLUMN_ANSWER_ID + " = "
            + ANSWER_TABLE_NAME + "." + ANSWER_COLUMN_ID
            + " WHERE "
            + Q_A_TABLE_NAME + "." + Q_A_COLUMN_TYPE_ID + " = \'" + type_id + "\'";

        if (!version_id.equals("0")) {
            query += " AND " + Q_A_TABLE_NAME + "." + Q_A_COLUMN_VERSION_ID + " = \'" + version_id + "\'";
        }

        if (bGroupByQuestionId) {
            query += " GROUP BY " + Q_A_TABLE_NAME + "." + Q_A_COLUMN_QUESTION_ID;
        }

        if (BuildConfig.DEBUG) {
            Log.i(
                TAG,
                "getQuestionAnswers( " + version_id + "," + type_id + "," + (bGroupByQuestionId ? "true": "false") + " )"
            );
            Log.i(
                TAG,
                query
            );
        }

        return database.rawQuery(
            query,
            null
        );
    }

    @Override
    public void onUpgrade(
        SQLiteDatabase db,
        int oldVersion,
        int newVersion
    ) {
        Log.w(
            DatabaseHelper.class.getName(),
            "Upgrading database from version "
                + oldVersion + " to " + newVersion + ", which will destroy all old data"
        );
        db.execSQL("DROP TABLE IF EXISTS " + QUESTION_DATABASE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + ANSWER_DATABASE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Q_A_DATABASE_TABLE);
        onCreate(db);
    }

}