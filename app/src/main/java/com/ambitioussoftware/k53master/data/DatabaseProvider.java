package com.ambitioussoftware.k53master.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.ambitioussoftware.k53master.BuildConfig;

import java.util.List;

public class DatabaseProvider extends ContentProvider {

    private static final String TAG = "DatabaseProvider";

    public static final String PROVIDER_NAME = "com.ambitioussoftware.k53master.data.DatabaseProvider";
    public static final String PROVIDER_MIME_TYPE_NAME = "proverbedk53Learners";
    public static final String CONTENT_URI_PATH = "content://" + PROVIDER_NAME + "/";
    public static final Uri CONTENT_URI = Uri.parse(CONTENT_URI_PATH);

    public static final String QUESTION_URI = "question";
    public static final String ANSWER_URI = "answer";
    public static final String Q_A_URI = "question_answer";

    public static final int QUESTION = 1;
    public static final int ANSWER = 2;
    public static final int QUESTION_ANSWER = 3;
    public static final int QUESTION_ANSWER_1 = 4;
    public static final int Q_A = 5;

    private static final UriMatcher uriMatcher = buildUriMatcher();

    public static final String DIR_BASE_MIME_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/"
        + PROVIDER_MIME_TYPE_NAME;
    public static final String ITEM_BASE_MIME_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/"
        + PROVIDER_MIME_TYPE_NAME;

    private SQLiteDatabase k53LearnersDb;

    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(
            PROVIDER_NAME,
            QUESTION_URI,
            QUESTION
        );
        matcher.addURI(
            PROVIDER_NAME,
            ANSWER_URI,
            ANSWER
        );
        matcher.addURI(
            PROVIDER_NAME,
            Q_A_URI + "/#" + "/#" + "/#",
            QUESTION_ANSWER
        );
        matcher.addURI(
            PROVIDER_NAME,
            Q_A_URI,
            QUESTION_ANSWER
        );
        matcher.addURI(
            PROVIDER_NAME,
            Q_A_URI + "/filter/*",
            QUESTION_ANSWER_1
        );
        return matcher;
    }

    @Override
    public int delete(
        Uri uri,
        String where,
        String[] selectionArgs
    ) {
        int count = 0;
        switch (uriMatcher.match(uri)) {

//            default:
//                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(
            uri,
            null
        );
        return count;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case QUESTION_ANSWER:
                return DIR_BASE_MIME_TYPE;
            case QUESTION_ANSWER_1:
                return DIR_BASE_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(
        Uri uri,
        ContentValues values
    ) {
        long rowID = 0L;

        switch (uriMatcher.match(uri)) {

//            default:
//                throw new IllegalArgumentException("Unsupported URI:" + uri);
        }

        if (rowID > 0) {
            Uri myUri = ContentUris.withAppendedId(
                uri,
                rowID
            );
            getContext().getContentResolver().notifyChange(
                myUri,
                null
            );
            return myUri;
        }
        throw new SQLException("Failed to insert row into " + uri);

    }

    @Override
    public boolean onCreate() {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(getContext());
        k53LearnersDb = dbHelper.getWritableDatabase();
        return (k53LearnersDb != null);
    }

    @Override
    public Cursor query(
        Uri uri,
        String[] columns,
        String where,
        String[] whereArgs,
        String sortOrder
    ) {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance(getContext());

        switch (uriMatcher.match(uri)) {
            case QUESTION_ANSWER:
                List<String> pathSegments = uri.getPathSegments();

                if (pathSegments.size() == 4) {
                    // @param version_id the version this is for, e.g. test set a, etc
                    String version_id = pathSegments.get(1);

                    // @param type_id the type this question and answers are for, e.g. motorcyle, etc
                    String type_id = pathSegments.get(2);

                    // @param bGroupByQuestionId whether we want to group by question id
                    boolean bGroupByQuestionId = pathSegments.get(3).equals("1");

                    return dbHelper.getQuestionAnswers(version_id, type_id, bGroupByQuestionId);
                } else {
                    SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

                    queryBuilder.setTables(DatabaseHelper.Q_A_TABLE_NAME);

                    Cursor cursor = queryBuilder.query(
                        dbHelper.getReadableDatabase(),
                        columns,
                        where,
                        whereArgs,
                        null,
                        null,
                        sortOrder
                    );
                    cursor.setNotificationUri(
                        getContext().getContentResolver(),
                        uri
                    );
                    return cursor;
                }
            default:
                throw new IllegalArgumentException("Unknown URI");
        }
    }

    @Override
    public int update(
        Uri uri,
        ContentValues values,
        String where,
        String[] selectionArgs
    ) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case QUESTION_ANSWER_1:
                List<String> pathSegments = uri.getPathSegments();
                String param = pathSegments.get(2);
                String customWhere = DatabaseHelper.Q_A_COLUMN_QUESTION_ID + " IN ( " + param + " )"
                    + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : "");
                count = k53LearnersDb.update(
                    DatabaseHelper.Q_A_TABLE_NAME,
                    values,
                    customWhere,
                    selectionArgs
                );
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI:" + uri);
        }
        getContext().getContentResolver().notifyChange(
            uri,
            null
        );
        return count;
    }

    public static Uri getContentUri(
        int uri,
        String... args
    ) {
        switch (uri) {
            case QUESTION:
                return Uri.parse(CONTENT_URI_PATH + QUESTION_URI);
            case ANSWER:
                return Uri.parse(CONTENT_URI_PATH + ANSWER_URI);
            case QUESTION_ANSWER:
                if (BuildConfig.DEBUG) {
                    Log.i(TAG, "getContentUri: QUESTION_ANSWER");
                }

                if (args != null) {
                    String ids = TextUtils.join("/", args);
                    if (BuildConfig.DEBUG) {
                        Log.i(TAG, "Uri.parse(" + CONTENT_URI_PATH + Q_A_URI + "/" + ids + ")");
                    }
                    return Uri.parse(CONTENT_URI_PATH + Q_A_URI + "/" + ids);
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.i(TAG, "Uri.parse(" + CONTENT_URI_PATH + Q_A_URI + ")");
                    }
                    return Uri.parse(CONTENT_URI_PATH + Q_A_URI);
                }
            case QUESTION_ANSWER_1:
                if (args != null) {
                    String ids = TextUtils.join("/", args);

                    return Uri.parse(CONTENT_URI_PATH + Q_A_URI + "/filter/" + ids);
                }
            default:
                throw new IllegalArgumentException("Unsupported URI:" + uri);
        }
    }

    public static Uri getContentUri(int uri) {
        return getContentUri(
            uri,
            new String[]{}
        );
    }

}
