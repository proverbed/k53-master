#!/bin/bash

# list all directories under ~/.android/avd
# loop through each directory and execute rm *.lock
find ~/.android/avd -name \*.lock -exec rm -f {} \;