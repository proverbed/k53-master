//+------------------------------------------------------------------+
//|                                                    proverbed.mq4 |
//|                                                  Dmitri De Klerk |
//|                                           dmitriwarren@gmail.com |
//+------------------------------------------------------------------+
#property copyright "Dmitri De Klerk"
#property link      "dmitriwarren@gmail.com"
#property version   "1.00"
//#property strict

extern double stopLossPipsFromX=0.001;
extern double Lots=0.01;
extern double acceptableRiskReward=1.2;
extern int Slippage=7;
extern string comment="proverbed";
extern int MagicNumber=0;
//+------------------------------------------------------------------+
//| Zig zag indicator parameters                                     |
//+------------------------------------------------------------------+
extern int iShiftBars=1;
extern int iZigZagDepth=10;     // Depth
extern int iZigZagDeviation=5;  // Deviation
extern int iZigZagBackstep=3;   // Backstep
double iZigZag=0;               // The current zig zag values, allowed to have a valid zero value
double iPrevZigZag=0.0;         // The previous zig zag values, this value is not allowed to be 0.0
//+------------------------------------------------------------------+
int ThisBarTrade=0;
int newBar=0;
int Position=0;
int Leg=0;
//+------------------------------------------------------------------+
//| These variables saves the different values of the gartley pattern|
//+------------------------------------------------------------------+

double xValue;
datetime xValueTime;
double aValue;
datetime aValueTime;
double bValue;
datetime bValueTime;
double cValue;
datetime cValueTime;
double dValue;
datetime dValueTime;
//+------------------------------------------------------------------+
//| These last valid leg of the gartley                              |
//+------------------------------------------------------------------+
double lastLegValue;
//+------------------------------------------------------------------+
//| These current leg of the gartley                                 |
//|   - X | A | B | C | D                                            |
//|   - 1 | 2 | 3 | 4 | 5                                            |
//+------------------------------------------------------------------+
int currentLeg=0;
//+------------------------------------------------------------------+
//| These direction of the trend                                     |
//|   - 1: up | 2: down                                              |
//+------------------------------------------------------------------+
int direction=0;
int prevDirection=0;
//+------------------------------------------------------------------+
//| Whether we want to log                                           |
//+------------------------------------------------------------------+
extern bool log=true;
double pattern[5];
bool highLow, prevHighLow;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//|                                                                  |
//+------------------------------------------------------------------+
int init() {
   CalcPosition();
   MathSrand(GetTickCount()); 
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| start                                                            |
//|                                                                  |
//+------------------------------------------------------------------+
int start() {
	bool closed=false;

		if(newBar!=Bars){

			// The current zig zag values, allowed to have a valid zero value
			iZigZag=getZigZag();

			if (log) {
				Print("iZigZag: ", iZigZag );
			}
	
			//Print("highLow: ", highLow, " prevHighLow: ", prevHighLow);
			if (iZigZag == 0) {
				if (log) {
					Print(" iZigZag == iPrevZigZag directions is still the same");
				}
				direction = prevDirection; // direction stays the same
			} else if (highLow && !prevHighLow) {
				if (log) {
					Print(iZigZag , " iZigZag > iPrevZigZag UP", iPrevZigZag);
				}
				direction = 1; // direction is up
			} else if (!highLow && prevHighLow) {
				if (log) {
					Print(iZigZag , " iZigZag < iPrevZigZag DOWN", iPrevZigZag);
				}
				direction = 2; // direction is down
			}

			switch(currentLeg) {
				case 0:
					// retracement occured
					if (direction != prevDirection && prevDirection != 0) {
						// Set the X leg
						currentLeg = 1; // set X leg
						xValue = iPrevZigZag;
						//iZigZag = iPrevZigZag; // set the iZigZag to the previous, so we can have a correct prev zig zag
						if (log) {
							Print("### ### ###  Setting X leg as ", DoubleToStr(xValue,Digits));
						}
					}

					break;
				case 1:
					// retracement occured
					if (direction != prevDirection && prevDirection != 0) {
						// Set the A leg
						currentLeg = 2; // set A leg
						aValue = iPrevZigZag;
						//iZigZag = iPrevZigZag; // set the iZigZag to the previous, so we can have a correct prev zig zag
						if (log) {
							Print("### ### ###  Setting A leg as ", DoubleToStr(aValue,Digits));
						}

						xValueTime = NULL;
						aValueTime = NULL;

						// Set the time values here
						if (xValue > aValue) {
						   // ReturnHighAsSeries(HIGH)
						   xValueTime = ReturnHighAsSeries(true, xValue);

						   // ReturnHighAsSeries(LOW)
						   aValueTime = ReturnHighAsSeries(false, aValue);
						} else if (xValue < aValue){
						   // ReturnHighAsSeries(LOW)
						   xValueTime = ReturnHighAsSeries(false, xValue);

						   // ReturnHighAsSeries(HIGH)
						   aValueTime = ReturnHighAsSeries(true, aValue);
						} else {
							Alert("X is the same as A", pattern[6]);
						}

					}

					break;
				case 2:
					// retracement occured, check if its valid
					if ( direction != prevDirection && prevDirection != 0) {

						if (checkRetracement(xValue, aValue, iPrevZigZag, 0.618, 0.786)) {
							// Set the B leg
							currentLeg = 3; // set B leg
							bValue = iPrevZigZag;
							//iZigZag = iPrevZigZag; // set the iZigZag to the previous, so we can have a correct prev zig zag
							if (log) {
								Print("### ### ###  Setting B leg as ", DoubleToStr(bValue,Digits));
							}

						} else {
							if (log) {
								Print("failedGartley: ", iPrevZigZag, " currentLeg: ", currentLeg);
							}
							failedGartley(iPrevZigZag);
						}
					}

					break;
				case 3:
					// retracement occured, check if its valid
					if ( direction != prevDirection && prevDirection != 0) {

						if (checkRetracement(aValue, bValue, iPrevZigZag, 0.618, 0.886)) {
							// Set the C leg
							currentLeg = 4; // set C leg
							cValue = iPrevZigZag;
							//iZigZag = iPrevZigZag; // set the iZigZag to the previous, so we can have a correct prev zig zag
							if (log) {
								Print("### ### ###  Setting C leg as ", DoubleToStr(cValue,Digits));
							}

							SendMail("Setting Gartley C Leg", StringConcatenate(" aValue: ", aValue, "bValue", bValue, "cValue", cValue, "dValue", dValue));

							bValueTime = NULL;
							cValueTime = NULL;

							// Set the time values here
							if (xValue > aValue) {
								// ReturnHighAsSeries(HIGH)
								bValueTime = ReturnHighAsSeries(true, bValue);

								// ReturnHighAsSeries(LOW)
								cValueTime = ReturnHighAsSeries(false, cValue);
							} else if (xValue < aValue){
								// ReturnHighAsSeries(LOW)
								bValueTime = ReturnHighAsSeries(false, bValue);

								// ReturnHighAsSeries(HIGH)
								cValueTime = ReturnHighAsSeries(true, cValue);
							} else {
								Alert("X is the same as A", pattern[6]);
							}

							Print(
								"xValueTime: ",TimeToStr(xValueTime,TIME_DATE|TIME_MINUTES),
								" aValueTime: ",TimeToStr(aValueTime,TIME_DATE|TIME_MINUTES),
								" bValueTime: ",TimeToStr(bValueTime,TIME_DATE|TIME_MINUTES),
								" cValueTime: ",TimeToStr(cValueTime,TIME_DATE|TIME_MINUTES)
							);

							int InpWidth=2; // Line width

							string InpName=StringConcatenate(TimeCurrent(), MathRand());
							ObjectCreate(ChartID(),InpName,OBJ_TRIANGLE,0,xValueTime,xValue,aValueTime,aValue,bValueTime,bValue);
							ObjectSetInteger(ChartID(),InpName,OBJPROP_COLOR,clrRed);
							ObjectSetInteger(ChartID(),InpName,OBJPROP_STYLE,STYLE_DASH);
							ObjectSetInteger(ChartID(),InpName,OBJPROP_WIDTH,InpWidth);

							setLabel("X", xValueTime, xValue);
							setLabel("A", aValueTime, aValue);
							setLabel("B", bValueTime, bValue);
							setLabel("C", cValueTime, cValue);

						} else {
							if (log) {
								Print("failedGartley: ", iPrevZigZag, " currentLeg: ", currentLeg);
							}
							failedGartley(iPrevZigZag);
						}
					}

					break;
				case 4:
					RefreshRates();
					double currentPrice = 0.0;

					// sell
					if (xValue > aValue) {
						currentPrice = Bid;
					}
					// buy
					else if (xValue < aValue){
						currentPrice = Ask;
					}
					if (log) {
						Print("calculateEntry(): ", calculateEntry(), " currentPrice:", currentPrice);
						Print("PARAMS xValue: ", xValue, " aValue: ", aValue, " bValue: ", bValue, " cValue: ", cValue, " dValue: ", dValue);
					}
					if (currentPrice == calculateEntry()) {
						// Set the D leg
						currentLeg = 5; // set D leg
						dValue = currentPrice;
						dValueTime = iTime(NULL, 0, 0);

						Print("****** ############### SUCCESSFUL GARTLEY ################## ****** ", currentPrice);
						Print("****** ############### SUCCESSFUL GARTLEY ################## ****** entry: ", calculateEntry());

						SendMail("EA found successfull buy Gartley", "****** ############### SUCCESSFUL GARTLEY ################## ******");

						if (xValue > aValue) {
							Print("****** ############### GARTLEY SELL ################## ******");

							OpenSingleSell(getStoploss(), 0.382, true); // exit .382
							OpenSingleSell(getStoploss(), 0.618, true); // exit .618

						} else if (xValue < aValue){
							Print("****** ############### GARTLEY BUY ################## ******");

							OpenSingleBuy(getStoploss(), 0.382, true); // exit .382
							OpenSingleBuy(getStoploss(), 0.618, true); // exit .618
						} else {
							Alert("X is the same as A", pattern[6]);
						}

						failedGartley(cValue);
					}

					// retracement occured
					if (direction != prevDirection && prevDirection != 0) {
						currentLeg = 5; // set D leg
						dValue = iPrevZigZag;

						if (log) {
							Print("failedGartley: ", iPrevZigZag, " currentLeg: ", currentLeg);
						}
						failedGartley(iPrevZigZag);
					}
					break;
				case 5:
					// close the order here maybe???
					break;
				default:
					Alert("Something has gone wrong");
			}
	
	   		// The previous zig zag values, this value is not allowed to be 0.0
	   		if (iZigZag != 0.0) {
				
				//Fix bug for getZigZag values, check if last high was the highest high
				//or check if last low was the lowest low
				if (highLow) {
					if (iZigZag > iPrevZigZag) {
						iPrevZigZag=iZigZag;
					}
				} else if (!highLow) {
					if (iZigZag < iPrevZigZag) {
						iPrevZigZag=iZigZag;
					}
				}

	   		}
	   		prevHighLow=highLow;
	   		prevDirection=direction;

			if (log) {
				Print("iPrevZigZag: ", iPrevZigZag);
				Print("prevDirection: ", prevDirection);
				Print("Direction: ", direction);
			}
			newBar=Bars;
		}
	
	// if we do have open positions we have to monitor 
	// we want to move stops when we need to
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Set the label for the gartley, A B C D                           |
//|                                                                  |
//+------------------------------------------------------------------+
void setLabel(string labelText, datetime time, double price) {
	string obj_name=StringConcatenate(time, price, MathRand());  
	ObjectCreate(ChartID(), obj_name,OBJ_TEXT,0,0,0);
	ObjectSetText(obj_name,labelText,10,"Times New Roman",Green);
	ObjectSet(obj_name, OBJPROP_TIME1, time); 
	ObjectSet(obj_name, OBJPROP_PRICE1, price); 

	string obj_name1=StringConcatenate(TimeToStr(TimeCurrent(),TIME_DATE|TIME_SECONDS), MathRand());  
	ObjectCreate(ChartID(), obj_name1,OBJ_ARROW_LEFT_PRICE,0,0,0);
	ObjectSetText(obj_name1,labelText,10,"Times New Roman",Green);
	ObjectSet(obj_name1, OBJPROP_TIME1, time); 
	ObjectSet(obj_name1, OBJPROP_PRICE1, price); 
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| getStoploss                                                      |
//|                                                                  |
//+------------------------------------------------------------------+
double getStoploss() {
	if (xValue > aValue) {
		return (NormalizeDouble(xValue + stopLossPipsFromX,Digits));
	} else if (xValue < aValue){
		return (NormalizeDouble(xValue - stopLossPipsFromX,Digits));
	} else {
		Alert("X is the same as A", pattern[6]);
	}
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Get the zig zag value and set the direction of the trend         |
//|                                                                  |
//+------------------------------------------------------------------+
double getZigZag() {
	double iZigZagHigh=iCustom(NULL,0,"ZigZag",iZigZagDepth,iZigZagDeviation,iZigZagBackstep,1,iShiftBars);
	double iZigZagLow=iCustom(NULL,0,"ZigZag",iZigZagDepth,iZigZagDeviation,iZigZagBackstep,2,iShiftBars);

	if (log) {
		Print("iZigZagHigh: ", DoubleToStr(iZigZagHigh,Digits), " iZigZagLow: ", DoubleToStr(iZigZagLow,Digits));
	}

	if (iZigZagHigh != 0.0) {
		if (iZigZagLow == 0.0) {
			if (log) {
				Print("getZigZag: ", iZigZagHigh, " high value");
			}
			highLow = true;
			return (NormalizeDouble(iZigZagHigh,Digits));
		} else if (iZigZagLow != 0.0) {
			if (log) {
				Print("getZigZag: ", iZigZagLow, " low value");
			}
			highLow = false;
			return (NormalizeDouble(iZigZagLow,Digits));
		} else {
			Alert("unknown scenario", pattern[6]);
		}
	} else if (iZigZagLow != 0.0) {
		if (iZigZagHigh == 0.0) {
			if (log) {
				Print("getZigZag: ", iZigZagLow, " low value");
			}
			highLow = false;
			return (NormalizeDouble(iZigZagLow,Digits));
		} else if (iZigZagHigh != 0.0) {
			if (log) {
				Print("getZigZag: ", iZigZagHigh, " high value");
			}
			highLow = true;
			return (NormalizeDouble(iZigZagHigh,Digits));
		} else {
			Alert("unknown scenario", pattern[6]);
		}

	} else {
		if (log) {
			Print("getZigZag: 0.0");
		}
		return (0.0);
	}
}

//+------------------------------------------------------------------+
//| Calculate the gartley entry                                      |
//|                                                                  |
//+------------------------------------------------------------------+
double calculateEntry(){
	
	double XArectracement = calculateRetracement(xValue, aValue, 0.786) ;
	double ABextension = calculateExtension(aValue, bValue, 1.272) ;
	
	/*
	// sell
	if (xValue > aValue) {
		
	} 
	// buy
	else if (xValue < aValue){
		
	} else {
		Alert("X is the same as A", pattern[6]);
	}
	*/
	Print("calculateEntry(): aValue: ", aValue, " bValue: ", bValue, " 1.272 extension: ", ABextension);	
	return (ABextension);
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Calculate a given retracement for a start and end leg            |
//|                                                                  |
//+------------------------------------------------------------------+
double calculateRetracement(double startLeg, double endLeg, double retracement) {
	if (log) {
		Print("calculateRetracement PARAMS startLeg: ", startLeg, " endLeg: ", endLeg, " retracement: ", retracement);	
	}

	double length = 0.0;
	double retracementValue = 0.0;

	if (startLeg > endLeg) {
		retracementValue = NormalizeDouble((startLeg - endLeg) * retracement,Digits);
		
		return NormalizeDouble(endLeg + retracementValue,Digits);
	} else if (startLeg < endLeg){
		retracementValue = NormalizeDouble((endLeg - startLeg) * retracement,Digits);

		return NormalizeDouble(endLeg - retracementValue,Digits);
	} else {
		Alert("The startLeg is equal to the endLeg.", pattern[6]);
		return (0.0);
	}
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Calculate a given extension for a start and end leg              |
//|                                                                  |
//+------------------------------------------------------------------+
double calculateExtension(double startLeg, double endLeg, double extension) {
	if (log) {
		Print("calculateExtension PARAMS startLeg: ", startLeg, " endLeg: ", endLeg, " extension: ", extension);	
	}

	double retracementValue = 0.0;

	if (startLeg > endLeg) {
		retracementValue = NormalizeDouble((startLeg - endLeg) * extension,Digits);

		return NormalizeDouble(startLeg - retracementValue,Digits);
	} else if (startLeg < endLeg){
		retracementValue = NormalizeDouble((endLeg - startLeg) * extension,Digits);

		return NormalizeDouble(startLeg + retracementValue,Digits);
	} else {
		Alert("The startLeg is equal to the endLeg.", pattern[6]);
		return (0.0);
	}
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Check if the retracement is within a range, and return true or   |
//| false depending on this.                                         |
//|                                                                  |
//+------------------------------------------------------------------+
bool checkRetracement(double startLeg, double endLeg, double retracementValue, double retraceRangeStart, double retraceRangeEnd, bool forceLog=false) {
	if (log || forceLog) {
		Print("checkRetracement PARAMS startLeg: ", startLeg, " endLeg: ", endLeg, " retracementValue: ", retracementValue, " retraceRangeStart: ", retraceRangeStart, " retraceRangeEnd: ", retraceRangeEnd, " forceLog: ", forceLog);
	}

	double length = 0.0;
	double retracement = 0.0;
	double retracementPercent = 0.0;

	if (startLeg > endLeg) {
		length = NormalizeDouble(startLeg - endLeg,Digits);
		
		if (retracementValue <= endLeg) {
			Alert("The retracement value: [", retracementValue, "] given is not valid retracement.", pattern[6]);
		} else {
			retracement = NormalizeDouble(retracementValue - endLeg,Digits);
		}

	} else if (startLeg < endLeg){
	
		length = NormalizeDouble(endLeg-startLeg,Digits);

		if (retracementValue >= endLeg) {
			Alert("The retracement value: [", retracementValue, "] given is not valid retracement.", pattern[6]);
		} else {
			retracement = NormalizeDouble(endLeg - retracementValue,Digits);
		}
	}
	retracementPercent = NormalizeDouble(retracement/length,Digits);
	if (log || forceLog) {
		Print("retracementValue: ", DoubleToStr(retracementValue,Digits));
		Print("length: ", DoubleToStr(length,Digits));
		Print("retracement: ", DoubleToStr(retracement,Digits));
		Print("retracementPercent: ", DoubleToStr(retracementPercent,Digits));
	}
	if ((retracementPercent >= retraceRangeStart) && (retracementPercent <= retraceRangeEnd)) {
		return (true);
	} else {
		return (false);
	}
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| The gartley pattern failed, we want to reset all variables at    |
//| this point, and set the x leg, of where the gartley must start   |
//| and try again.                                                   |
//|                                                                  |
//+------------------------------------------------------------------+
void failedGartley(double xLeg) {
	if (log) {
		Print("###### FAILED GARTLEY ######");
		Print("failedGartley PARAMS xValue: ", xValue, " aValue: ", aValue, " bValue: ", bValue, " cValue: ", cValue, " dValue: ", dValue);
	}
	aValue = 0;
	bValue = 0;
	cValue = 0;
	dValue = 0;
	
	currentLeg = 1; // set X leg
	xValue = xLeg;
	Print("### ### ###  Setting X leg as ", DoubleToStr(xValue,Digits));
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Return the date time for a given price, so that we can draw      |
//| the gartley pattern                                              |
//|                                                                  |
//+------------------------------------------------------------------+
datetime ReturnHighAsSeries(bool high, double searchValue){
	RefreshRates();
	datetime TimeAsSeries[];
	double HighLowAsSeries[];
	int count = 50;
	 
	//--- set access to the array like to a timeseries
	ArraySetAsSeries(TimeAsSeries,true);
	ArraySetAsSeries(HighLowAsSeries,true);

	ResetLastError();
	int copied=CopyTime(NULL,0,0,count,TimeAsSeries);
	if(copied<=0) {
		Print("The copy operation of the open time values for last 20 bars has failed");
		return;
	}
	if (high) {
		Print("ReturnHighAsSeries(HIGH): ", "searchValue: ", DoubleToStr(searchValue,Digits));
		copied=CopyHigh(Symbol(),0,0,count,HighLowAsSeries);
	} else {
		Print("ReturnHighAsSeries(LOW): ", "searchValue: ", DoubleToStr(searchValue,Digits));
		copied=CopyLow(Symbol(),0,0,count,HighLowAsSeries);
	}
	if(copied<=0) {
		Print("The copy operation of the high/low values for last 20 bars has failed");
		return;
	}

	int size=ArraySize(TimeAsSeries);

	Print("0 HighLowAsSeries[0] =", DoubleToStr(HighLowAsSeries[0], Digits)); 
	Print("TimeAsSeries[0] =",TimeToStr(TimeAsSeries[0],TIME_DATE|TIME_MINUTES));

	for(int i=0;i<size;i++) {
		if ( HighLowAsSeries[i] == searchValue ) {
			return (NormalizeDouble(TimeAsSeries[i], Digits));
		}
	}
	return (NULL);
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//|                                                                  |
//+------------------------------------------------------------------+
void CalcPosition() {
   Position=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
      if(OrderSymbol()==Symbol())
        {
         if(OrderType() == OP_BUY) Position += 1;
         if(OrderType() == OP_SELL) Position -=1;
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//|                                                                  |
//+------------------------------------------------------------------+
int OpenSingleBuy(double stoploss=0.0, double takeprofit=0.382, bool override = false){
   int ticket=-1;
   if(Position != 0) return(ticket);
   while(!IsTradeAllowed()) Sleep(MathRand()/10);
   Print("OpenSingleBuy PARAMS xValue: ", xValue, " aValue: ", aValue, " bValue: ", bValue, " cValue: ", cValue, " dValue: ", dValue);
   RefreshRates();
   double adSwing = NormalizeDouble(aValue - dValue,Digits); 
   takeprofit = NormalizeDouble(Ask + (adSwing * takeprofit),Digits);
   if (calculateRiskReward(Ask, stoploss, takeprofit) || override) {
	   //Print("adSwing: ", adSwing, " Ask: ", Ask, " stoploss: ", DoubleToStr(stoploss,Digits), " takeprofit: ", DoubleToStr(takeprofit,Digits));	
	   ticket=OrderSend(Symbol(),OP_BUY,Lots,Ask,Slippage,stoploss,takeprofit,comment+" buy",MagicNumber,0,Lime);
	   if(ticket<0){
	      Print("Failed to OpenSingleBuy, error # ",GetLastError());
	      return (ticket);
	   } else {
	      Print("Successfully placed order with OpenSingleBuy, ticket: ", ticket);
	      return (ticket);
	   }
   }
   return (ticket);
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| OpenSingleSell                                                   |
//|                                                                  |
//+------------------------------------------------------------------+
int OpenSingleSell(double stoploss=0.0, double takeprofit=0.382, bool override = false) {
	Print("OpenSingleSell PARAMS xValue: ", xValue, " aValue: ", aValue, " bValue: ", bValue, " cValue: ", cValue, " dValue: ", dValue);
   int ticket=-1;
   if(Position != 0) return(ticket);
   while(!IsTradeAllowed()) Sleep(MathRand()/10);
   RefreshRates();
   double adSwing = NormalizeDouble(dValue - aValue,Digits); 
   takeprofit = NormalizeDouble(Bid - (adSwing * takeprofit),Digits);
   if (calculateRiskReward(Bid, stoploss, takeprofit) || override) {
	   //Print("adSwing: ", adSwing, " Bid: ", Bid, " stoploss: ", DoubleToStr(stoploss,Digits), " takeprofit: ", DoubleToStr(takeprofit,Digits));	
	   ticket=OrderSend(Symbol(),OP_SELL,Lots,Bid,Slippage,stoploss,takeprofit,comment+" sell",MagicNumber,0,Lime);
	   if(ticket<0) {
	      Print("Failed to OpenSingleSell, error # ",GetLastError());
	      return (ticket);
	   } else {
	      Print("Successfully placed order with OpenSingleSell, ticket: ", ticket);
	      return (ticket);
	   }
   }
   return (ticket);
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Calculate whether the rist to reward is acceptable               |
//| based on the external acceptableRiskReward value.                |
//|                                                                  |
//+------------------------------------------------------------------+
bool calculateRiskReward(double entry, double stop, double takeprofit) {
	double risk, reward = 0.0; 
	if (log) {
		Print("calculateRiskReward PARAMS entry: ", DoubleToStr(entry, 8), " stop: ", DoubleToStr(stop, 8), " takeprofit: ", DoubleToStr(takeprofit, 8));
	}

	// SELL
	if (stop > entry) {
		risk = NormalizeDouble(stop - entry,Digits); 
		
		if (takeprofit >= entry) {
			Print("take profit needs to be less than entry for a sell - takeprofit: ", takeprofit, " entry: ", entry);
			Alert("take profit needs to be less than entry for a sell", pattern[6]);
		} else {
			reward = NormalizeDouble(entry - takeprofit,Digits);
		}
	} 

	// BUY
	else if(stop < entry) {
		risk = NormalizeDouble(entry - stop,Digits); 
		
		if (NormalizeDouble(entry,Digits) >= NormalizeDouble(takeprofit,Digits)) {
			Print("entry needs to be less than take profit for a buy - takeprofit: ", takeprofit, " entry: ", entry);
			Alert("entry needs to be less than take profit for a buy", pattern[6]);
		} else {
			reward = NormalizeDouble(takeprofit - entry,Digits);
		}
	} else {
		Print("stop: ", stop, " entry: ", entry);
		Alert("stop and entry is the same.", pattern[6]);
	}
	Print("risk: ", risk, " reward: ", reward, "acceptableRiskReward: ", acceptableRiskReward);
	if (reward/risk > acceptableRiskReward) {
		return (true);
	} else {
		SendMail("RISK REWARD NOT ACCEPTABLE, not taking the trade", StringConcatenate(" risk: ", risk, " reward: ", reward));
		Print("# RISK REWARD NOT ACCEPTABLE, not taking the trade #");	
		return (false);
	}
}
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| ISSUES                                                           |
//|                                                                  |
//+------------------------------------------------------------------+
//|
//| * Check when (iHigh/iLow) is the same as getZigZag
//| * - Get the turning point immediately
//| * 
//| * Trade all gartleys, afterwards filter on: 
//| * - risk == reward
//| * - filtering on structure % (ranking) 
//| * - take stats on timeframes (5M, 15M, 30M, H1, H4)
//| * - take stats on different zig zag parameters
//| * 
//| * Chart Issues
//| * - 2015.11.12 11:30 - M15 - GBPUSD - start x time is null
//| * - 2016.03.08 07:00 - H1 - GBPUSD - x is not on the zigzag value
//| * 
//| * - 2016.01.01 - 2016.03.11 - H1 - GBPUSD
//| * 
//+------------------------------------------------------------------+
